<?php
session_start();
require_once 'config/config.php';
require_once BASE_PATH . '/includes/auth_validate.php';
require_once BASE_PATH . '/lib/General/Producto.php';
$obra = new Producto();
// Get current page
$page = filter_input(INPUT_GET, 'page');
if (!$page) {
    $page = 1;
}

// Get DB instance. i.e instance of MYSQLiDB Library
$db = getDbInstance();
$select = array('id_obra', 'obra', 'tipo_trabajo', 'hora_inicio_obra', 'hora_fin_obra', 'duracion', 'observaciones_obra', 'id_users', 'horas_estimadas');

$sesion_id_user = $_SESSION['id_user'];


// Get result of the query
$rows = $db->get('obra');
$permisos = [];
if (isset($_SESSION['permisos'])) {
    $permisos = explode(',', $_SESSION['permisos']);
}

?>
<?php include BASE_PATH . '/includes/header.php'; ?>

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Obra</h1>
        <?php if ($_SESSION['is_admin'] || (in_array('add', $permisos))): ?>
            <a href="add_obra.php" class="d-none d-sm-inline-block btn btn-sm btn-dark shadow-sm">
                <i class="fas fa-plus fa-sm text-white-50"></i> Crear obra
            </a>
        <?php endif; ?>
    </div>
<?php include BASE_PATH . '/includes/flash_messages.php'; ?>

    <!-- Table -->
    <div style="margin-left: 15px">
    <table class="table table-bordered table-scroll" id="table-obra">
        <thead>
        <tr>
            <th>Obra</th>
            <th>Nº pedido</th>
            <th>Artículos</th>
            <th>Tipo Trabajo</th>
            <th>H. incio</th>
            <th>H. fin</th>
            <th>Duración</th>
            <th>Observaciones</th>
            <th>Trabajador</th>
            <?php if ($_SESSION['is_admin'] || (in_array('delete', $permisos) || in_array('edit', $permisos))): ?>
                <th>Acciones</th>
            <?php endif; ?>
        </tr>
        </thead>
        <tbody>
        <?php
        $totalHoras = [];
        foreach ($rows as $row):
            $idUsers = explode(',', $row['id_users']);
            if ((in_array($_SESSION['id_user'], $idUsers) && $row['show_obra']) || $_SESSION['is_admin']):
                $productoData = '';
                if (!is_null($row['productos_obra']) || $row['productos_obra'] != '') {
                    $productoData = $db->rawQuery("SELECT id_producto, producto, num_pedido_cliente FROM producto 
                    WHERE id_producto IN ({$row['productos_obra']})");
                }
                ?>
                <tr onclick="showTotals(<?= $row['id_obra'] ?>)">
                    <td>
                        <?php echo htmlspecialchars($row['obra']); ?>
                    </td>
                    <td>

                        <?php
                        if (!empty($productoData)) {
                            foreach ($productoData as $product) {
                                echo $product['num_pedido_cliente'] . '<br>';
                            }
                        } else {
                            echo "No existe";
                        } ?>
                    </td>
                    <td>
                        <?php if (!empty($productoData)) {
                            foreach ($productoData as $productname) {
                                $totalHoras[$row['id_obra']][] = $productname['id_producto'];
                                echo $productname['producto'] . '<br>';
                            }
                        } else {
                            echo "No existe";
                        }
                        //calculamos totales
                        if (!empty($totalHoras[$row['id_obra']])) {
                            $explodeProductos = implode(',', $totalHoras[$row['id_obra']]);

                            $datosAsignadaTallerProducto = $db->rawQuery("
                            SELECT 
                                   SUM(hp.horas_fabrica) as horas_asignadas
                            FROM producto p
                            INNER JOIN horas_produccion hp ON p.id_horas = hp.id_horas 
                            WHERE p.id_producto IN ({$explodeProductos})");

                            $datosAsignadaTallerProducto = reset($datosAsignadaTallerProducto);

                            $horasAsignadas = round($datosAsignadaTallerProducto['horas_asignadas'], 2) ?? 0;
                            $totalHoras[$row['id_obra']]['horas_totales'] = $horasAsignadas;
                            $totalHoras[$row['id_obra']]['horas_estimadas'] = $row['horas_estimadas'];
                            $totalHoras[$row['id_obra']]['diferencia'] = abs($horasAsignadas - $row['horas_estimadas']);
                        }
                        ?>
                    </td>
                    <td>
                        <?php echo htmlspecialchars($row['tipo_trabajo']); ?>
                    </td>
                    <td>
                        <?php echo htmlspecialchars(date("H:i", strtotime($row['hora_inicio_obra']))); ?>
                    </td>
                    <td>
                        <?php echo htmlspecialchars(date("H:i", strtotime($row['hora_fin_obra']))); ?>
                    </td>
                    <?php
                    $diferenciaHoras = $db->rawQuery("SELECT TIMEDIFF(hora_fin_obra,hora_inicio_obra) 
                as duracion FROM obra WHERE id_obra = {$row['id_obra']}");

                    $diferenciaHoras = reset($diferenciaHoras);
                    $duracion = $diferenciaHoras['duracion'];

                    ?>

                    <td>
                        <?php echo htmlspecialchars($duracion); ?>
                    </td>
                    <td>
                        <?php echo htmlspecialchars($row['observaciones_obra']); ?>
                    </td>
                    <?php

                    $trabajadoresObra = [];
                    if ($row['id_users'] != '') {
                        $trabajadoresObra = $db->rawQuery("
                        SELECT id_user, user_name 
                        FROM users 
                        WHERE id_user IN ({$row['id_users']})");
                    }
                    ?>
                    <td>
                        <?php if (!empty($trabajadoresObra)) {
                            foreach ($trabajadoresObra as $trabajadores) {
                                echo $trabajadores['user_name'] . '<br>';
                            }
                        } else {
                            echo "No existe";
                        } ?>

                    </td>
                    <?php if ($_SESSION['is_admin'] || (in_array('delete', $permisos) || in_array('edit', $permisos))): ?>
                        <td>
                            <?php if ($_SESSION['is_admin']): ?>
                                <a href="#" data-toggle="modal"
                                   data-target="#trabajadores-<?php echo $row['id_obra']; ?>"
                                   class="btn btn-info btn-sm">
                                    <i class="fas fa-user" aria-hidden="true"></i>
                                </a>
                            <?php endif; ?>
                            <?php if ($_SESSION['is_admin'] || in_array('edit', $permisos)): ?>
                                <a href="edit_obra.php?id_obra=<?php echo $row['id_obra']; ?>&operation=edit"
                                   class="btn btn-info btn-sm">
                                    <i class="fas fa-edit" aria-hidden="true"></i>
                                </a>
                            <?php endif; ?>
                            <?php if ($_SESSION['is_admin'] || in_array('delete', $permisos)): ?>
                                <a href="#" data-toggle="modal"
                                   data-target="#confirm-delete-<?php echo $row['id_obra']; ?>"
                                   class="btn btn-info btn-sm">
                                    <i class="fas fa-trash" aria-hidden="true"></i>
                                </a>
                            <?php endif; ?>
                        </td>
                    <?php endif; ?>
                </tr>
                <?= generatePopUpDelete($row['id_obra'], 'obra', 'delete_obra.php') ?>
                <?= generatePopUpTrabajadores($row['id_obra'], 'trabajadores', 'asociar_trabajador_obra.php', $row['id_users']) ?>
            <?php endif;
        endforeach; ?>
        </tbody>
    </table>
    <container>
        <b>**Para consultar horas clicke en la fila correspondiente</b>
        <div style="height: 190px; width:250px">
            <div>
                <!-- seems to work even without flex-column :D -->
                <div class="border border-primary bg-primary text-white m-2">Total horas <span id="total_horas">0</span>
                </div>
                <div class="border border-primary bg-primary text-white m-2">Total horas estimadas <span
                            id="total_horas_estimadas">0</span></div>
                <div class="border border-success bg-success text-white m-2">Diferencia <span id="diferencia">0</span>
                </div>
            </div>
            <div/>
    </container>

    <script>
        $(document).ready(function () {
            // Setup - add a text input to each footer cell
            $('#table-obra thead tr')
                .clone(true)
                .addClass('filters')
                .appendTo('#table-obra thead');

            var table = $('#table-obra').dataTable({
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: ':not(:last-child)',
                        }
                    },
                    {
                        extend: 'csvHtml5',
                        exportOptions: {
                            columns: ':not(:last-child)',
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        exportOptions: {
                            columns: ':not(:last-child)',
                        }
                    }
                ],
                "scrollX": true,
                "language": {
                    "info": "Página _PAGE_ de _PAGES_",
                    "search": "Buscar",
                    "lengthMenu": "Mostrar _MENU_ registros",
                    "infoEmpty": "Sin registros",
                    "paginate": {
                        "previous": "Primero",
                        "next": "Último"
                    },
                    "infoFiltered": " - filtrado en _MAX_ registros",
                    "zeroRecords": "No se encontraron resultados al filtrar"
                },
                orderCellsTop: true,
                fixedHeader: true,
                initComplete: function () {
                    var api = this.api();

                    // For each column
                    api
                        .columns()
                        .eq(0)
                        .each(function (colIdx) {
                            // Set the header cell to contain the input element
                            var cell = $('.filters th').eq(
                                $(api.column(colIdx).header()).index()
                            );
                            var title = $(cell).text();
                            $(cell).html('<input type="text" placeholder="Buscar" />');

                            // On every keypress in this input
                            $(
                                'input',
                                $('.filters th').eq($(api.column(colIdx).header()).index())
                            )
                                .off('keyup change')
                                .on('keyup change', function (e) {
                                    e.stopPropagation();

                                    // Get the search value
                                    $(this).attr('title', $(this).val());
                                    var regexr = '({search})'; //$(this).parents('th').find('select').val();

                                    var cursorPosition = this.selectionStart;
                                    // Search the column for that value
                                    api
                                        .column(colIdx)
                                        .search(
                                            this.value != ''
                                                ? regexr.replace('{search}', '(((' + this.value + ')))')
                                                : '',
                                            this.value != '',
                                            this.value == ''
                                        )
                                        .draw();

                                    $(this)
                                        .focus()[0]
                                        .setSelectionRange(cursorPosition, cursorPosition);

                                });
                        });

                },
            });

        });

        function showTotals(id) {
            var arrayFromPHP = <?php echo json_encode($totalHoras); ?>;
            if (arrayFromPHP[id] !== undefined) {
                $('#total_horas').text(arrayFromPHP[id]['horas_totales']);
                $('#total_horas_estimadas').text(arrayFromPHP[id]['horas_estimadas']);
                $('#diferencia').text(arrayFromPHP[id]['diferencia']);
            } else {
                $('#total_horas').text(0);
                $('#total_horas_estimadas').text(0);
                $('#diferencia').text(0);
            }
        }
    </script>

<?php include BASE_PATH . '/includes/footer.php'; ?>