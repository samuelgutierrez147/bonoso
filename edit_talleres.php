<?php
//Inicio sesion
session_start();
//Acceso a la configuracion
require_once 'config/config.php';
//Clase validadora
require_once BASE_PATH . '/includes/auth_validate.php';

//class
require_once BASE_PATH . '/lib/General/Talleres.php';
$taller = new Talleres();

//Llamos a un valor externo, el get se recoge a traves de la url PARAMETROS DE LA URL
$id_taller = filter_input(INPUT_GET, 'id_taller');
$operation = filter_input(INPUT_GET, 'operation', FILTER_SANITIZE_STRING);

($operation == 'edit') ? $edit = true : $edit = false;

// Serve POST request
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    // Sanitize input post if we want
    $data_to_db = filterDataPost();
    $db = getDbInstance();

    //Reset db instance

    $db = getDbInstance();
    $db->where('id_taller', $id_taller,'=');
    $stat = $db->update("talleres", $data_to_db);

    if ($stat) {
        $_SESSION['success'] = 'Taller<b> con el ID ' . $id_taller . '</b> actualizado';
    } else {
        $_SESSION['failure'] = 'Algo falló al actualizar el taller: ' . $db->getLastError();
    }
    header('location: talleres.php');
    exit;

}

$db = getDbInstance();
$db->where('id_taller', $id_taller);
$edit_talleres = $db->getOne("talleres");

$db->where('id_taller', $id_taller);
$db->orderBy('anio','ASC');
$db->orderBy('semana','ASC');
$semanasTaller = $db->get("semanas_taller");

?>
<?php include BASE_PATH . '/includes/header.php'; ?>
<!-- Begin Page Content -->
<div class="container-fluid">
    <?php include BASE_PATH . '/includes/flash_messages.php'; ?>
    <?php include BASE_PATH . '/forms/talleres_form.php'; ?>
</div>
<?php include BASE_PATH . '/includes/footer.php'; ?>
