<?php
session_start();
require_once 'config/config.php';
require_once BASE_PATH . '/includes/auth_validate.php';

// Users class
require_once BASE_PATH . '/lib/General/Talleres.php';
$taller = new Talleres();
$edit = false;

// Serve POST request
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    // Sanitize input post if we want
    $data_to_db = filterDataPost();

    // Reset db instance
    $db = getDbInstance();
    try {
        $last_id = $db->insert('talleres', $data_to_db);
        if ($last_id) {
            $_SESSION['success'] = 'Creado correctamente';
            header('location: talleres.php');
            exit;
        }else{
            $_SESSION['failure'] = $e->getMessage();
        }
    } catch (Exception $e) {
        $_SESSION['failure'] = $e->getMessage();
    };

}
?>
<?php include BASE_PATH . '/includes/header.php'; ?>
<div class="container" style="margin-top: 15px;">
    <div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">Añadir</h2>
        </div>
    </div>
    <?php include BASE_PATH . '/includes/flash_messages.php'; ?>
    <form class="well form-horizontal" action="" method="post" id="contact_form" enctype="multipart/form-data">
        <div class="col">
            <div class="col-lg-6">
                <label for="nombre_taller" class="form-label">Taller</label>
                <input type="text" class="form-control" id="nombre_taller" name="nombre_taller" required>
            </div>
            <div class="col-12" style="margin-top: 20px">
                <button type="submit" class="btn btn-primary">Confirmar</button>
            </div>
        </div>
    </form>
<?php include BASE_PATH . '/includes/footer.php'; ?>