<?php
session_start();
require_once 'config/config.php';
require_once BASE_PATH . '/includes/auth_validate.php';
require_once BASE_PATH . '/lib/General/Talleres.php';
$taller = new Talleres();
// Get current page
$page = filter_input(INPUT_GET, 'page');
if (!$page) {
    $page = 1;
}

// Get DB instance. i.e instance of MYSQLiDB Library
$db = getDbInstance();
$select = array('id_taller', 'nombre_taller');

// Get result of the query
$rows = $db->get('talleres');

$month_start = strtotime('first day of this month', time());
$fechaInicio = date('Y-m-d', $month_start);
$month_end = strtotime('last day of this month', time());
$fechaFin = date('Y-m-d', $month_end);
?>
<?php include BASE_PATH . '/includes/header.php'; ?>
    <!-- Main container -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Talleres con horas del mes actual (<?= date('M') ?>)</h1>
            <a href="add_talleres.php" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                        class="fas fa-plus fa-sm text-white-50"></i> Añadir nuevo</a>
        </div>
        <?php include BASE_PATH . '/includes/flash_messages.php'; ?>

        <!-- Table -->
        <div style="overflow-x: auto;">
            <table class="table table-bordered table-scroll" id="table-talleres">
            <thead>
            <tr>
                <th>Talleres</th>
                <th>Incremento horas</th>
                <th>Total capacidad del taller</th>
                <th>Diferencia horas</th>
                <th>Acciones</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($rows as $row):
                $semanasMes = getWeeksFromMonthAndYear(date('M'));

                $datosCapacidadTallerProducto = $db->rawQuery("
                    SELECT 
                        SUM(s.horas_trabajo_dia * s.dias_trabajo_semana * s.num_trabajadores) as horas_capacidad
                    FROM semanas_taller s
                    INNER JOIN talleres t ON s.id_taller = t.id_taller
                    WHERE s.id_taller = {$row['id_taller']} 
                    AND s.semana IN ({$semanasMes})
                    AND s.anio = '" . date('Y') . "'");

                $datosAsignadaTallerProducto = $db->rawQuery("
                    SELECT 
                           SUM(hp.horas_fabrica) as horas_asignadas
                    FROM producto p
                    INNER JOIN fabricacion f ON f.id_fabricacion = p.id_fabricacion
                    INNER JOIN horas_produccion hp ON p.id_horas = hp.id_horas 
                    WHERE p.id_taller = {$row['id_taller']} 
                    AND WEEK(f.fecha_ped_compra_fabricacion) IN ({$semanasMes})
                    AND YEAR(f.fecha_ped_compra_fabricacion) = '" . date('Y') . "'
                    AND p.estado = 'en fabricacion'");

                $datosCapacidadTallerProducto = reset($datosCapacidadTallerProducto);
                $datosAsignadaTallerProducto = reset($datosAsignadaTallerProducto);

                $horasCapacidad = round($datosCapacidadTallerProducto['horas_capacidad'],2) ?? 0;
                $horasAsignadas = round($datosAsignadaTallerProducto['horas_asignadas'],2) ?? 0;
                $diferenciaHoras = $horasCapacidad - $horasAsignadas;

            ?>
                <tr>
                    <td><?php echo htmlspecialchars($row['nombre_taller']); ?></td>
                    <td><?php echo htmlspecialchars(($horasAsignadas > $horasCapacidad) ? abs($diferenciaHoras) : 0); ?></td>
                    <td><?php echo htmlspecialchars($horasCapacidad); ?></td>
                    <td><?php echo htmlspecialchars($diferenciaHoras); ?></td>
                    <td>
                        <a href="edit_talleres.php?id_taller=<?php echo $row['id_taller']; ?>&operation=edit"
                           class="btn btn-info btn-sm">
                            <i class="fas fa-edit" aria-hidden="true"></i>
                        </a>
                        <a href="#" data-toggle="modal"
                           data-target="#confirm-delete-<?php echo $row['id_taller']; ?>"
                           class="btn btn-info btn-sm">
                            <i class="fas fa-trash" aria-hidden="true"></i>
                        </a>
                    </td>
                </tr>
                <!-- Delete Confirmation Modal -->
                <?= generatePopUpDelete($row['id_taller'], 'talleres', 'delete_talleres.php') ?>
            <?php endforeach; ?>
            </tbody>
        </table>
        </div>
    </div>

<script>
    $(document).ready(function () {
        // Setup - add a text input to each footer cell
        $('#table-talleres thead tr')
            .clone(true)
            .addClass('filters')
            .appendTo('#table-talleres thead');

        var table = $('#table-talleres').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
            "language": {
                "info": "Página _PAGE_ de _PAGES_",
                "search": "Buscar",
                "lengthMenu": "Mostrar _MENU_ registros",
                "infoEmpty": "Sin registros",
                "paginate": {
                    "previous": "Primero",
                    "next": "Último"
                },
                "infoFiltered": " - filtrado en _MAX_ registros",
                "zeroRecords": "No se encontraron resultados al filtrar"
            },
            orderCellsTop: true,
            fixedHeader: true,
            initComplete: function () {
                var api = this.api();

                // For each column
                api
                    .columns()
                    .eq(0)
                    .each(function (colIdx) {
                        // Set the header cell to contain the input element
                        var cell = $('.filters th').eq(
                            $(api.column(colIdx).header()).index()
                        );
                        var title = $(cell).text();
                        $(cell).html('<input type="text" placeholder="Buscar" />');

                        // On every keypress in this input
                        $(
                            'input',
                            $('.filters th').eq($(api.column(colIdx).header()).index())
                        )
                            .off('keyup change')
                            .on('keyup change', function (e) {
                                e.stopPropagation();

                                // Get the search value
                                $(this).attr('title', $(this).val());
                                var regexr = '({search})'; //$(this).parents('th').find('select').val();

                                var cursorPosition = this.selectionStart;
                                // Search the column for that value
                                api
                                    .column(colIdx)
                                    .search(
                                        this.value != ''
                                            ? regexr.replace('{search}', '(((' + this.value + ')))')
                                            : '',
                                        this.value != '',
                                        this.value == ''
                                    )
                                    .draw();

                                $(this)
                                    .focus()[0]
                                    .setSelectionRange(cursorPosition, cursorPosition);
                            });
                    });
            },
        });
    });
</script>

<?php include BASE_PATH . '/includes/footer.php'; ?>