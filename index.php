<?php
session_start();
require_once './config/config.php';
require_once 'includes/auth_validate.php';

//Get DB instance. function is defined in config.php
$db = getDbInstance();

//Get Dashboard information
$users = $db->getValue("users", "count(*)");

include_once('includes/header.php');
?>
<?php include BASE_PATH . '/includes/flash_messages.php'; ?>
<?php include_once('includes/footer.php'); ?>
