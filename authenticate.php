<?php
require_once 'config/config.php';
session_start();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $username = filter_input(INPUT_POST, 'username');
    $password = filter_input(INPUT_POST, 'password');
    $remember = filter_input(INPUT_POST, 'remember');

    // Get DB instance.
    $db = getDbInstance();

    $db->where('user_name', $username);
    try {
        $row = $db->getOne('users');

        if ($db->count >= 1) {
            $db_password = $row['password'];
            $id_user = $row['id_user'];

            if (password_verify($password, $db_password)) {
                $_SESSION['user_logged_in'] = TRUE;
                //get if is admin
                $_SESSION['is_admin'] = ($row['user_type'] == 'admin') ?? false;

                $_SESSION['id_user'] = $row['id_user'];
                $_SESSION['user_name'] = $row['user_name'];
                $_SESSION['permisos'] = $row['permisos'];

                if ($remember) {
                    $series_id = randomString(16);
                    $remember_token = getSecureRandomToken(20);
                    $encryted_remember_token = password_hash($remember_token, PASSWORD_DEFAULT);

                    $expiry_time = date('Y-m-d H:i:s', strtotime(' + 30 days'));
                    $expires = strtotime($expiry_time);

                    setcookie('series_id', $series_id, $expires, '/');
                    setcookie('remember_token', $remember_token, $expires, '/');

                    $db = getDbInstance();
                    $db->where('id_user', $id_user);

                    $update_remember = [
                        'series_id' => $series_id,
                        'remember_token' => $encryted_remember_token,
                        'expires' => $expiry_time
                    ];
                    try {
                        $db->update('users', $update_remember);
                    } catch (Exception $e) {
                        echo $e->getCode()." ".$e->getMessage();
                    }
                }
                // Authentication successfull redirect user
                if(!$_SESSION['is_admin']){
                    header('Location: obra.php');
                    exit;
                }

                header('Location: inicio.php');
            } else {
                $_SESSION['login_failure'] = 'Usuario o contraseña incorrectos';
                header('Location: login.php');
            }
            exit;
        } else {
            $_SESSION['login_failure'] = 'Usuario o contraseña incorrectos';
            header('Location: login.php');
            exit;
        }
    } catch (Exception $e) {
        echo $e->getCode()." ".$e->getMessage();
    }
} else {
    die('Method Not allowed');
}
