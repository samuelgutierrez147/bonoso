<?php
session_start();
require_once 'config/config.php';
require_once BASE_PATH . '/includes/auth_validate.php';

// Users class
require_once BASE_PATH . '/lib/Users/Users.php';
$users = new Users();

// User ID for which we are performing operation
$id_user = filter_input(INPUT_GET, 'id_user');
$operation = filter_input(INPUT_GET, 'operation', FILTER_SANITIZE_STRING);
//REFACTORING
if (($_SESSION['id_user'] != $id_user) && !$_SESSION['is_admin']) {
    $_SESSION['failure'] = 'No tiene acceso';
    header('location: index.php');
    exit;
}
($operation == 'edit') ? $edit = true : $edit = false;

// Serve POST request
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    // Sanitize input post if we want
    $data_to_db = filterDataPost();

    $db = getDbInstance();
    $db->where('user_name', $data_to_db['user_name']);
    $db->where('id_user', $id_user, '!=');

    $row = $db->getOne('users');
    if (!empty($row['user_name'])) {
        $_SESSION['failure'] = 'Usuario ya existe';
        $query_string = http_build_query(array(
            'id_user' => $id_user,
            'operation' => $operation,
        ));
        header('location: edit_user.php?' . $query_string);
        exit;
    }

    $id_user = filter_input(INPUT_GET, 'id_user', FILTER_VALIDATE_INT);
    // Encrypting the password
    $data_to_db['password'] = password_hash($data_to_db['password'], PASSWORD_DEFAULT);
    // Reset db instance
    $db = getDbInstance();
    $db->where('id_user', $id_user, '=');
    $stat = $db->update('users', $data_to_db);

    if ($stat) {
        $_SESSION['success'] = 'Usuario <b>' . $data_to_db['user_name'] . '</b> actualizado';
    } else {
        $_SESSION['failure'] = 'Algo falló al actualizar el usuario: ' . $db->getLastError();
    }
    //REFACTORING
    if ($_SESSION['is_admin']) {
        header('location: users.php');
    } else {
        header('location: index.php');
    }

    exit;
}

// Select where clause
$db = getDbInstance();
$db->where('id_user', $id_user);
$admin_account = $db->getOne("users");
?>
<?php include BASE_PATH . '/includes/header.php'; ?>
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Editar <b><?= $_SESSION['user_name'] ?></b></h1>
    </div>

    <form class="well form-horizontal" action="" method="post" id="contact_form" enctype="multipart/form-data">
        <?php include BASE_PATH . '/forms/users_form.php'; ?>
    </form>
</div>
<?php include BASE_PATH . '/includes/footer.php'; ?>
