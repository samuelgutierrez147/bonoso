<?php
session_start();
require_once 'config/config.php';
require_once BASE_PATH . '/includes/auth_validate.php';
include BASE_PATH . '/includes/header.php';

global $reboseGraficaHorasAsignadas;
global $mesReboseGraficaHorasAsignadas;
global $anioReboseGraficaHorasAsignadas;

$db = getDbInstance();
$meses = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
$datosGraficaHorasAsignadasPorcentaje = [];
$datosGraficaHorasAsignadasAsignadas = [];
$datosGraficaHorasAsignadasCapacidad = [];
$datosGraficaHorasAsignadasRestantes = [];
$datosGraficaHorasPendientesPorcentaje = [];
$datosGraficaHorasPendientesRestantes = [];
$datosGraficaHorasPendientesAsignadas = [];
$datosGraficaHorasPendientesCapacidad = [];
$datosGraficaTalleres = [];
$datosGraficaTallerGrafica = [];
$mesActual = (int)date('m');
$acumulacionAsignadasAsignadas = 0;
$acumulacionPendientesAsignadas = 0;
$acumulacionPorTaller = [];
$acumulacionGraficaTaller = [];

foreach ($meses as $key => $mes) {
    $mesInt = array_search($mes, $meses) + 1;
    $pedidosYaAsignados = getPedidosYaAsignados($mes);
    $pedidosAsignadosMasPendientes = getPedidosYaAsignadosMasPendientes($mes);

    if ($mesInt < $mesActual) {
        array_push($datosGraficaHorasAsignadasPorcentaje, 0);
        array_push($datosGraficaHorasAsignadasRestantes, 100);
        $acumulacionAsignadasAsignadas += $pedidosYaAsignados['horas_asignadas'];

        array_push($datosGraficaHorasPendientesPorcentaje, 0);
        array_push($datosGraficaHorasPendientesRestantes, 100);
        $acumulacionPendientesAsignadas += $pedidosAsignadosMasPendientes['horas_asignadas'];

        // ---------------------------------------------------------------------------------------------------//
        array_push($datosGraficaTalleres, getDatosMesTaller($mes, null, true));
        $acumulacionPorTaller[] = getDatosMesTaller($mes);

        // ---------------------------------------------------------------------------------------------------//
        array_push($datosGraficaTallerGrafica, getPedidosYaAsignadosMesGrafica($mes, null, true));
        $acumulacionGraficaTaller[] = getPedidosYaAsignadosMesGrafica($mes);

    } else {
        if ($mesInt == $mesActual) {
            $pedidosYaAsignados = getPedidosYaAsignados($mes, null, null, $acumulacionAsignadasAsignadas);
            $pedidosAsignadosMasPendientes = getPedidosYaAsignadosMasPendientes($mes, null, $acumulacionPendientesAsignadas);
        }
        array_push($datosGraficaHorasAsignadasPorcentaje, $pedidosYaAsignados['porcentaje']);
        array_push($datosGraficaHorasAsignadasRestantes, $pedidosYaAsignados['restante']);
        array_push($datosGraficaHorasAsignadasAsignadas, $pedidosYaAsignados['horas_asignadas']);
        array_push($datosGraficaHorasAsignadasCapacidad, $pedidosYaAsignados['horas_capacidad']);

        array_push($datosGraficaHorasPendientesPorcentaje, $pedidosAsignadosMasPendientes['porcentaje']);
        array_push($datosGraficaHorasPendientesRestantes, $pedidosAsignadosMasPendientes['restante']);
        array_push($datosGraficaHorasPendientesAsignadas, $pedidosAsignadosMasPendientes['horas_asignadas']);
        array_push($datosGraficaHorasPendientesCapacidad, $pedidosAsignadosMasPendientes['horas_capacidad']);

        // ---------------------------------------------------------------------------------------------------//
        array_push($datosGraficaTalleres, getDatosMesTaller($mes));

        // ---------------------------------------------------------------------------------------------------//
        array_push($datosGraficaTallerGrafica, getPedidosYaAsignadosMesGrafica($mes));
    }
}

if (!empty($acumulacionPorTaller)) {
    foreach ($acumulacionPorTaller as $mes => $datosPorTaller) {
        foreach ($datosPorTaller as $tallerId => $datos) {
            if(!isset($datosGraficaTalleres[$mesActual - 1][$tallerId]))
                $datosGraficaTalleres[$mesActual - 1][$tallerId] = ['horas_asignadas' => 0];

            if(!isset($datosGraficaTalleres[$mesActual - 1][$tallerId]['horas_capacidad']))
                $datosGraficaTalleres[$mesActual - 1][$tallerId]['horas_capacidad'] = 0;

            $datosGraficaTalleres[$mesActual - 1][$tallerId]['horas_asignadas'] += $datos['horas_asignadas'];
        }
    }
}

if (!empty($acumulacionGraficaTaller)) {
    foreach ($acumulacionGraficaTaller as $mes => $datosPorTaller) {
        foreach ($datosPorTaller as $tallerId => $datos) {
            if(!isset($datosGraficaTallerGrafica[$mesActual - 1][$tallerId]))
                $datosGraficaTallerGrafica[$mesActual - 1][$tallerId] = ['horas_asignadas' => 0];

            if(!isset($datosGraficaTallerGrafica[$mesActual - 1][$tallerId]['horas_capacidad']))
                $datosGraficaTallerGrafica[$mesActual - 1][$tallerId]['horas_capacidad'] = 0;

            $datosGraficaTallerGrafica[$mesActual - 1][$tallerId]['horas_asignadas'] += $datos['horas_asignadas'];

            $horasAsignadas = $datosGraficaTallerGrafica[$mesActual - 1][$tallerId]['horas_asignadas'];
            $horasCapacidad = $datosGraficaTallerGrafica[$mesActual - 1][$tallerId]['horas_capacidad'];

            $porcentaje = ($horasAsignadas > 0 && $horasCapacidad > 0) ? round(($horasAsignadas / $horasCapacidad) * 100) : 0;
            $porcentaje = ($porcentaje > 99) ? 100 : $porcentaje;
            $restante = 100 - $porcentaje;

            $datosGraficaTallerGrafica[$mesActual - 1][$tallerId]['porcentaje'] = number_format((float)$porcentaje, 2, '.', '');
            $datosGraficaTallerGrafica[$mesActual - 1][$tallerId]['restante'] = number_format((float)$restante, 2, '.', '');
        }
    }
}

$tipo = applyDataAnualByDate($datosGraficaHorasAsignadasPorcentaje,
    $datosGraficaHorasAsignadasRestantes,
    $datosGraficaHorasPendientesPorcentaje,
    $datosGraficaHorasPendientesRestantes,
    $datosGraficaTalleres,
    $datosGraficaTallerGrafica,
    $datosGraficaHorasAsignadasAsignadas,
    $datosGraficaHorasAsignadasCapacidad,
    $datosGraficaHorasPendientesAsignadas,
    $datosGraficaHorasPendientesCapacidad);

//Para calcular rebose
applyReboseGraficaTaller($datosGraficaTallerGrafica, $tipo);

processData($datosGraficaHorasAsignadasPorcentaje,
    $datosGraficaHorasAsignadasRestantes,
    $datosGraficaHorasAsignadasAsignadas,
    $datosGraficaHorasAsignadasCapacidad,
    $tipo);

processData($datosGraficaHorasPendientesPorcentaje,
    $datosGraficaHorasPendientesRestantes,
    $datosGraficaHorasPendientesAsignadas,
    $datosGraficaHorasPendientesCapacidad,
    $tipo);

processDataTaller($datosGraficaTallerGrafica,
    $tipo);

?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css"/>


<div class="container">
    <h2>Horas de cada taller</h2>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <?php
            $tallerData = $db->rawQuery("SELECT id_taller,nombre_taller FROM talleres");
            ?>
            <select class="selectpicker" id="selectpicker" onchange="mostrarGraficaTaller(this.value)"
                    data-show-subtext="true" data-live-search="true">
                <option value="" data-toggle="modal">Seleccione taller</option>

                <?php foreach ($tallerData as $taller): ?>
                    <option value="<?= $taller['id_taller'] ?>"><?= $taller['nombre_taller']; ?></option>
                <?php endforeach; ?>

            </select>
            <div id="grafica" style="margin-top: 100px; text-align: right"></div>
        </div>
        <div class="col-lg-6 col-md-6" style="height:300px !important;">
            <h2>Pedidos ya asignados por taller</h2>
            <canvas id="chart3"></canvas>
        </div>
    </div>
</div>

<div class="container" style="margin-top: 100px;">
    <div class="row">
        <div class="col-lg-6 col-md-6" style="height:300px !important;">
            <h2>Pedidos ya asignados</h2>
            <canvas id="chart1"></canvas>
        </div>
        <div class="col-lg-6 col-md-6" style="height:300px !important;">
            <h2>Ya asignados + pendientes</h2>
            <canvas id="chart2"></canvas>
        </div>
    </div>
</div>

<script>
    var date = new Date(),
        month = date.getMonth(),
        year = date.getFullYear(),
        monthsLabel = ['Enero ' + year, 'Febrero ' + year, 'Marzo ' + year, 'Abril ' + year, 'Mayo ' + year, 'Junio ' + year, 'Julio ' + year, 'Agosto ' + year, 'Septiembre ' + year,
            'Octubre ' + year, 'Noviembre ' + year, 'Diciembre ' + year];

    if (month === 0 || month === 1) {
        month += 2;
        monthsLabel.unshift('Noviembre ' + (year - 1), 'Diciembre ' + (year - 1));
    } else if (month === 10 || month === 11) {
        monthsLabel.push('Enero ' + (year + 1), 'Febrero ' + (year + 1));
    }

    var ctx = document.getElementById("chart1").getContext("2d"),
        myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: monthsLabel,
                datasets: [{
                    label: 'Ocupadas %',
                    backgroundColor: "#caf270",
                    data: <?= json_encode($datosGraficaHorasAsignadasPorcentaje)?>,
                }, {
                    label: 'Libre %',
                    backgroundColor: "#45c490",
                    data: <?= json_encode($datosGraficaHorasAsignadasRestantes)?>,
                }],
            },
            options: {
                tooltips: {
                    displayColors: true,
                    callbacks: {
                        mode: 'x',
                    },
                },
                scales: {
                    xAxes: [{
                        stacked: true,
                        gridLines: {
                            display: false,
                        }
                    }],
                    yAxes: [{
                        stacked: true,
                        ticks: {
                            beginAtZero: true,
                            max: 100
                        },
                        type: 'linear',
                    }]
                },
                responsive: true,
                maintainAspectRatio: false,
                legend: {position: 'bottom'},
            }
        });

    var ctxchart2 = document.getElementById("chart2").getContext("2d"),
        myChart2 = new Chart(ctxchart2, {
            type: 'bar',
            data: {
                labels: monthsLabel,
                datasets: [{
                    label: 'Ocupadas %',
                    backgroundColor: "#caf270",
                    data: <?= json_encode($datosGraficaHorasPendientesPorcentaje)?>,
                }, {
                    label: 'Libre %',
                    backgroundColor: "#45c490",
                    data: <?= json_encode($datosGraficaHorasPendientesRestantes)?>,
                }],
            },
            options: {
                tooltips: {
                    displayColors: true,
                    callbacks: {
                        mode: 'x',
                    },
                },
                scales: {
                    xAxes: [{
                        stacked: true,
                        gridLines: {
                            display: false,
                        }
                    }],
                    yAxes: [{
                        stacked: true,
                        ticks: {
                            beginAtZero: true,
                            max: 100
                        },
                        type: 'linear',
                    }]
                },
                responsive: true,
                maintainAspectRatio: false,
                legend: {position: 'bottom'},
            }
        });

    function mostrarGraficaTaller(id_taller) {
        var datosGraficaJson = <?= json_encode($datosGraficaTalleres)?>,
            datosGraficaTallerGrafica = <?= json_encode($datosGraficaTallerGrafica)?>,
            arrayPorcentaje = [],
            arrayRestantes = [];

        $('#grafica').empty();

        $.each(datosGraficaJson, function (key, value) {
            $.each(value, function (key2, data) {
                if (parseInt(id_taller) === parseInt(key2) && parseInt(key) === parseInt(month)) {
                    var horasLibres = (parseFloat(data.horas_capacidad) - parseFloat(data.horas_asignadas) > 0 ?
                        parseFloat(data.horas_capacidad) - parseFloat(data.horas_asignadas) :
                        0);

                    $('#grafica').append('<h4><b>Datos de taller mes actual</b></h4>' +
                        '<div class="col-sm-12" style="text-align: right"><b>Capacidad:</b> ' + data.horas_capacidad.toFixed(2) +
                        '</div>' +
                        '<div class="col-sm-12" style="text-align: right">' +
                        '<b>Asignadas:</b> ' + data.horas_asignadas.toFixed(2) +
                        '</div>' +
                        '<div class="col-sm-12" style="text-align: right">' +
                        '<b>Libres:</b> ' + horasLibres.toFixed(2) +
                        '</div>' +
                        '</div>');
                }
            });
        });


        $.each(datosGraficaTallerGrafica, function (key, value) {
            arrayPorcentaje.push(value[id_taller]['porcentaje']);
            arrayRestantes.push(value[id_taller]['restante']);
        });

        var ctxchart3 = document.getElementById("chart3").getContext("2d");
        var myChart3 = new Chart(ctxchart3, {
            type: 'bar',
            data: {
                labels: monthsLabel,
                datasets: [{
                    label: 'Ocupadas %',
                    backgroundColor: "#caf270",
                    data: arrayPorcentaje,
                }, {
                    label: 'Libre %',
                    backgroundColor: "#45c490",
                    data: arrayRestantes,
                }],
            },
            options: {
                tooltips: {
                    displayColors: true,
                    callbacks: {
                        mode: 'x',
                    },
                },
                scales: {
                    xAxes: [{
                        stacked: true,
                        gridLines: {
                            display: false,
                        }
                    }],
                    yAxes: [{
                        stacked: true,
                        ticks: {
                            beginAtZero: true,
                            max: 100
                        },
                        type: 'linear',
                    }]
                },
                responsive: true,
                maintainAspectRatio: false,
                legend: {position: 'bottom'},
            }
        });
    }
</script>
<!-- //Main container -->
<?php include BASE_PATH . '/includes/footer.php'; ?>

