<?php
//Inicio sesion
session_start();
//Acceso a la configuracion
require_once 'config/config.php';
//Clase validadora
require_once BASE_PATH . '/includes/auth_validate.php';

// Serve POST request
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    // Sanitize input post if we want
    $data_to_db = filterDataPost();

    //Reset db instance
    $db = getDbInstance();

    $data_to_db['anio'] = ($data_to_db['semana']) ? date('Y', strtotime($data_to_db['semana'])) : '';
    $data_to_db['semana'] = ($data_to_db['semana']) ? date('W', strtotime($data_to_db['semana'])) : '';

    $id_taller = $data_to_db['id_taller'];

    $db->where('id_taller', $id_taller);
    $db->where('semana', $data_to_db['semana']);
    $db->where('anio', $data_to_db['anio']);

    $tallerAnioSemana = $db->getOne('semanas_taller', 'id_semanas');
    if ($tallerAnioSemana['id_semanas']) {
        $db->where('id_semanas', $tallerAnioSemana['id_semanas']);
        $stat = $db->update("semanas_taller", $data_to_db);
    } else {
        $stat = $db->insert("semanas_taller", $data_to_db);
    }

    if ($stat) {
        $_SESSION['success'] = 'Semana de taller creada';
    } else {
        $_SESSION['failure'] = 'Algo falló al actualizar el taller: ' . $db->getLastError();
    }
    header('location: edit_talleres.php?id_taller=' . $id_taller . '&operation=edit');
    exit;

}