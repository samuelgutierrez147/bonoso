<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-bold"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Bonoso</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">Gestión</div>
    <!-- Estructura personalizada de cliente -->
    <!-- Nav Item - Utilities Collapse Menu -->

    </a>
    <li class="nav-item">
        <?php if($_SESSION['is_admin']): ?>
            <a class="nav-link collapsed" href="inicio.php">
                <i class="fas fa-home"></i>
                <span>Gráfica</span>
            </a>

            <a class="nav-link collapsed" href="obra.php">
                <i class="fas fa-hammer"></i>
                <span>Obra</span>
                </a>

            <a class="nav-link collapsed" href="producto.php">
                <i class="fas fa-building"></i>
                <span>Producto</span>
            </a>
            <a class="nav-link collapsed" href="talleres.php">
                <i class="fas fa-wrench"></i>
                <span>Talleres</span>
            </a>
            <a class="nav-link collapsed" href="users.php">
                <i class="fas fa-user"></i>
                <span>Usuarios</span>
            </a>


        <?php else: ?>
            <a class="nav-link" href="obra.php">
                <i class="fas fa-hammer"></i>
                <span>Obra</span>
            </a>
        <?php endif; ?>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
<!-- End of Sidebar -->