<?php
session_start();
require_once 'includes/auth_validate.php';
require_once './config/config.php';
$del_id = filter_input(INPUT_POST, 'del_id');
$db = getDbInstance();

if(!$_SESSION['is_admin']){
    header('HTTP/1.1 401 Unauthorized', true, 401);
    exit("401 Unauthorized");
}


// Delete a user using id_user
if ($del_id && $_SERVER['REQUEST_METHOD'] == 'POST') {
    $db->where('id_producto', $del_id);
    $productData = $db->getOne('producto','id_taller');
    $idTaller = $productData['id_taller'];
    $sqlQuery = $db->query("SELECT fecha_ped_compra_fabricacion FROM fabricacion f
    INNER JOIN producto p ON f.id_fabricacion = p.id_fabricacion WHERE p.id_producto = {$del_id}");
    $sqlQuery = reset($sqlQuery);

    $db->where('id_producto', $del_id);
    $stat = $db->delete('producto');
    if ($stat) {
        $_SESSION['success'] = "Producto borrado correctamente";
        header('location:producto.php');
        exit;
    }
}