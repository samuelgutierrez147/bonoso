<?php
session_start();
require_once 'includes/auth_validate.php';
require_once './config/config.php';
$estado = filter_input(INPUT_POST, 'estado');
$taller = filter_input(INPUT_POST, 'id_taller');
$id_producto = filter_input(INPUT_POST, 'id_producto');
$db = getDbInstance();

if ($id_producto && $estado && $taller && $_SERVER['REQUEST_METHOD'] == 'POST') {
    $db->where('id_producto', $id_producto);

    if($estado == '1')
        $estado = 'pendiente subcont';

    $stat = $db->update('producto', ['estado' => $estado, 'id_taller' => $taller]);
    if ($stat) {
        $_SESSION['success'] = "Producto modificado correctamente";
        header('location:producto.php');
        exit;
    }
}