<form class="well form-horizontal" action="" method="post" id="contact_form" enctype="multipart/form-data">
    <div class="container">
        <div class="row">
            <form class="well form-horizontal" action="" method="post" id="contact_form" enctype="multipart/form-data">
                <div class="col">
                    <h3>Editar</h3>
                    <div class="col-lg-6">
                        <label for="nombre_taller" class="form-label">Taller</label>
                        <input type="text" class="form-control" id="nombre_taller" name="nombre_taller" required
                               value="<?php if (isset($operation) && $operation == 'edit') {
                                   echo $edit_talleres['nombre_taller'];
                               } ?>">
                    </div>
                    <div class="col-12" style="margin-top: 20px">
                        <button type="submit" class="btn btn-primary">Confirmar</button>
                    </div>
                </div>
            </form>
            <div class="col">
                <form class="well form-horizontal" action="add_week_taller.php" method="post" id="add_week_taller"
                      enctype="multipart/form-data">
                    <h3>Añadir/modificar semana</h3>
                    <div class="col-lg-6">
                        <label for="num_trabajadores" class="form-label">Nº trabajadores</label>
                        <input type="number" class="form-control" name="num_trabajadores" id="num_trabajadores">
                    </div>
                    <div class="col-lg-6">
                        <label for="semana" class="form-label">Nº semana</label>
                        <input type="week" class="form-control" name="semana" id="semana">
                    </div>
                    <div class="col-lg-6">
                        <label for="dias_trabajo_semana" class="form-label">Dias a la semana</label>
                        <input type="number" class="form-control" name="dias_trabajo_semana" id="dias_trabajo_semana">
                    </div>
                    <div class="col-lg-6">
                        <label for="horas_trabajo_dia" class="form-label">Horas dias</label>
                        <input type="number" class="form-control" name="horas_trabajo_dia" id="horas_trabajo_dia">
                    </div>
                    <div class="col-12" style="margin-top: 20px">
                        <button type="submit" class="btn btn-primary">Añadir</button>
                    </div>
                    <input type="hidden" name="id_taller" value="<?= $id_taller ?>">
                </form>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="col-lg-12" style="margin-top: 50px">
            <table class="table table-bordered table-scroll" id="table-semanas-taller">
                <thead>
                <tr>
                    <th>Año</th>
                    <th>Semana</th>
                    <th>Nº trabajadores</th>
                    <th>Dias semana</th>
                    <th>Horas dias</th>
                    <th>Incremento horas</th>
                    <th>Total capacidad del taller</th>
                    <th>Diferencia horas</th>
                </tr>
                </thead>
                <tbody>
                <?php $horasRebose = [];
                $semanaSql = 0;
                $anioSql = (int)date('Y');
                foreach ($semanasTaller as $sTaller) :
                    if ($sTaller['semana'] < $semanaSql && ($semanaSql > 0 && $sTaller['semana'] > 0)) {
                        $anioSql++;
                    }
                    $datosAsignadasTallerProducto = $db->rawQuery("
                    SELECT 
                           SUM(hp.horas_fabrica) as horas_asignadas
                    FROM producto p
                    INNER JOIN fabricacion f ON f.id_fabricacion = p.id_fabricacion
                    INNER JOIN horas_produccion hp ON p.id_horas = hp.id_horas 
                    WHERE p.id_taller = {$id_taller}
                    AND WEEK(f.fecha_ped_compra_fabricacion) = {$sTaller['semana']}
                    AND YEAR(f.fecha_ped_compra_fabricacion) = '" . $anioSql . "'
                    AND p.estado = 'en fabricacion'
                    GROUP BY WEEK(f.fecha_ped_compra_fabricacion)");

                    $datosCapacidadTallerProducto = $db->rawQuery("
                    SELECT 
                        s.horas_trabajo_dia as horas_capacidad,
                        s.dias_trabajo_semana as dias_semana
                    FROM semanas_taller s
                    INNER JOIN talleres t ON s.id_taller = t.id_taller
                    WHERE s.id_taller = {$id_taller}
                    AND s.semana = {$sTaller['semana']}
                    AND s.anio = '" . $anioSql . "'");

                    $datosAsignadasTallerProducto = reset($datosAsignadasTallerProducto);
                    $datosCapacidadTallerProducto = reset($datosCapacidadTallerProducto);

                    $diasSemana = $datosCapacidadTallerProducto['dias_semana'];
                    $horasAsignadas = round($datosAsignadasTallerProducto['horas_asignadas'], 2) ?? 0;
                    $totalHoras = round($sTaller['horas_trabajo_dia'] * $sTaller['dias_trabajo_semana'] * $sTaller['num_trabajadores'], 2);
                    $diferenciaHoras = $totalHoras - $horasAsignadas;

                    $semana = (($sTaller['semana'] + 1) == 53) ? 1 : $sTaller['semana'] + 1;
                    $anio = (($sTaller['semana'] + 1) == 53) ? $sTaller['anio'] + 1 : $sTaller['anio'];
                    $horasRebose[$anio][$semana] = ($horasAsignadas > $totalHoras) ? abs($diferenciaHoras) : 0;

                    $horasReboseRow = 0;
                    if (array_key_exists(intval($sTaller['anio']), $horasRebose)) {
                        if (array_key_exists(intval($sTaller['semana']), $horasRebose[$sTaller['anio']])) {
                            $horasReboseRow = $horasRebose[$sTaller['anio']][intval($sTaller['semana'])];
                            $totalHoras-=$horasReboseRow;
                        }
                    }
                    ?>
                    <tr>
                        <td><?php echo htmlspecialchars($sTaller['anio']); ?></td>
                        <td><?php echo htmlspecialchars($sTaller['semana']); ?></td>
                        <td><?php echo htmlspecialchars($sTaller['num_trabajadores']); ?></td>
                        <td><?php echo htmlspecialchars($sTaller['dias_trabajo_semana']); ?></td>
                        <td><?php echo htmlspecialchars($sTaller['horas_trabajo_dia']); ?></td>
                        <td><?php echo htmlspecialchars(($horasAsignadas > $totalHoras) ? abs($diferenciaHoras) : 0); ?></td>
                        <td><?php echo htmlspecialchars($totalHoras); ?></td>
                        <td><?php echo htmlspecialchars(($horasAsignadas < $totalHoras) ? abs($diferenciaHoras - $horasReboseRow) : 0); ?></td>
                    </tr>
                    <?php
                    $semanaSql = $sTaller['semana'];
                endforeach;?>
                </tbody>
            </table>
        </div>
    </div>