<?php
$db = getDbInstance();
$estado = $db->rawQuery("SELECT estado FROM producto ");
?>

<div class="container container-presupuesto-general-header">
    <div class="card mb-6">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">General</h6>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col form-group">
                <label for="num_pedido_cliente" class="form-label">Nº Ped. Cliente</label>
                <input type="text" class="form-control" id="num_pedido_cliente" name="num_pedido_cliente" required
                       value="<?php if (isset($operation) && $operation == 'edit') {
                           echo $edit_producto['num_pedido_cliente'];
                       } ?>">
                </div>
                <div class="col form-group">
                    <label for="fecha_ped_cliente" class="form-label">Fecha Ped. Cliente</label>
                    <input type="datetime-local" class="form-control" name="fecha_ped_cliente" id="fecha_ped_cliente"
                           value="<?php if (isset($operation) && $operation == 'edit') {
                               echo date('Y-m-d', strtotime($edit_producto['fecha_ped_cliente'])) . 'T' . date('H:i', strtotime($edit_producto['fecha_ped_cliente']));
                           } ?>">
                </div>
                <div class="col form-group">
                    <label for="cliente" class="form-label">Cliente</label>
                    <input type="text" class="form-control" name="cliente" id="cliente"
                           value="<?php if (isset($operation) && $operation == 'edit') {
                               echo $edit_producto['cliente'];
                           } ?>">
                </div>
                <div class="col form-group">
                    <label for="asunto" class="form-label">Asunto</label>
                    <input type="text" class="form-control" name="asunto" id="asunto"
                           value="<?php if (isset($operation) && $operation == 'edit') {
                               echo $edit_producto['asunto'];
                           } ?>">
                </div>
                <div class="col form-group">
                    <label for="referencia" class="form-label">Referencia</label>
                    <input type="text" class="form-control" id="referencia" name="referencia"
                           value="<?php if (isset($operation) && $operation == 'edit') {
                               echo $edit_producto['referencia'];
                           } ?>">
                </div>
            </div>
            <div class="row">
                <div class="col form-group">
                    <label for="num_unidad" class="form-label">Num. unidad</label>
                    <input type="number" min="0" class="form-control" name="num_unidad" id="num_unidad"
                           value="<?php if (isset($operation) && $operation == 'edit') {
                               echo $edit_producto['num_unidad'];
                           } ?>">
                </div>
                <div class="col form-group">
                    <label for="producto" class="form-label">Producto</label>
                    <input type="text" class="form-control" name="producto" id="producto"
                           value="<?php if (isset($operation) && $operation == 'edit') {
                               echo $edit_producto['producto'];
                           } ?>">
                </div>
                <div class="col form-group">
                    <label for="ref_articulo" class="form-label">Ref. Articulo</label>
                    <input type="text" min="0" class="form-control" name="ref_articulo" id="ref_articulo"
                           value="<?php if (isset($operation) && $operation == 'edit') {
                               echo $edit_producto['ref_articulo'];
                           } ?>">
                </div>
                <div class="col form-group">
                    <label for="gestor" class="form-label">Gestor</label>
                    <input type="text" min="0" class="form-control" name="gestor" id="gestor"
                           value="<?php if (isset($operation) && $operation == 'edit') {
                               echo $edit_producto['gestor'];
                           } ?>">
                </div>
                <div class="col form-group">

                    <label for="estado" class="form-label">Estado</label>
                    <select name="estado" class="form-control" id="estado">
                        <option value="">Seleccione estado</option>
                        <option value="pendiente subcont"<?php if (isset($operation) && $operation == 'edit' && $edit_producto['estado'] == 'pendiente subcont') {
                            echo ' selected';
                        } ?>>pendiente subcont
                        </option>
                        <option value="en fabricacion"<?php if (isset($operation) && $operation == 'edit' && $edit_producto['estado'] == 'en fabricacion') {
                            echo ' selected';
                        } ?>>en fabricacion
                        </option>
                        <option value="en pulimento"<?php if (isset($operation) && $operation == 'edit' && $edit_producto['estado'] == 'en pulimento') {
                            echo ' selected';
                        } ?>>pulimento
                        </option>
                        <option value="en carga"<?php if (isset($operation) && $operation == 'edit' && $edit_producto['estado'] == 'en carga') {
                            echo ' selected';
                        } ?>>taller complemento
                        </option>
                        <option value="terminado"<?php if (isset($operation) && $operation == 'edit' && $edit_producto['estado'] == 'terminado') {
                            echo ' selected';
                        } ?>>terminado
                        </option>
                    </select>
                </div>
                
            </div>



        </div>
    </div>
</div>
<div class="container">
    <div class="card mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Datos del producto</h6>
        </div>
        <div class="card-body">
            <nav>
                <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist" style="font-size: 10px;">
                    <a class="nav-item nav-link" id="pd_0e_elem" data-toggle="tab" href="#nav-0e_elem"
                       role="tab" aria-controls="nav-0e_elem" aria-selected="true">Horas de producción</a>
                    <a class="nav-item nav-link" id="pd_2e_elem" data-toggle="tab" href="#nav-2e_elem"
                       role="tab" aria-controls="nav-2e_elem" aria-selected="true">Fabricación</a>
                    <a class="nav-item nav-link " id="pd_3e_elem" data-toggle="tab" href="#nav-3e_elem"
                       role="tab" aria-controls="nav-3e_elem" aria-selected="true">Pulimento</a>
                    <a class="nav-item nav-link " id="pd_4e_elem" data-toggle="tab" href="#nav-4e_elem"
                       role="tab" aria-controls="nav-4e_elem" aria-selected="true">Taller complemento</a>
                </div>
            </nav>
            <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                <div class="tab-pane fade show" id="nav-0e_elem" role="tabpanel" aria-labelledby="0e_elem">
                    <div class="row column-0">
                        <div class="col form-group">
                            <div class="col form-group"><label for="horas_fabrica" class="form-label">Horas Fabrica</label>
                                <input type="number" min="0" step="0.01" class="form-control" name="horas_fabrica"
                                       id="horas_fabrica"
                                    <?php if (isset($operation) && $operation == 'edit' && $horasData): ?>
                                        value="<?= $horasData['horas_fabrica'] ?>"
                                    <?php endif; ?>></div>
                            <div class="col form-group"><label for="horas_pulimento"
                                                               class="form-label">Horas Pulimento</label>
                                <input type="number" step="0.01" class="form-control" name="horas_pulimento"
                                       id="horas_pulimento"
                                       value="<?php if (isset($operation) && $operation == 'edit' && $horasData) {
                                           echo $horasData['horas_pulimento'];
                                       } ?>"></div>

                            <div class="col form-group"><label for="horas_fabricacion_y_pulimento" class="form-label">Horas de embalaje</label>
                                <input type="number" step="0.01" class="form-control" name="horas_fabricacion_y_pulimento"
                                       id="horas_fabricacion_y_pulimento"
                                       value="<?php if (isset($operation) && $operation == 'edit' && $horasData) {
                                           echo $horasData['horas_fabricacion_y_pulimento'];
                                       } ?>"></div>
                            <div class="col form-group"><label for="horas_mont_ext" class="form-label">Horas montaje exterior</label>
                                <input type="number" step="0.01" class="form-control" name="horas_mont_ext"
                                       id="horas_mont_ext"
                                       value="<?php if (isset($operation) && $operation == 'edit' && $horasData) {
                                           echo $horasData['horas_mont_ext'];
                                       } ?>"></div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade show" id="nav-2e_elem" role="tabpanel" aria-labelledby="2e_elem">
                    <div class="row column-0">
                        <div class="col form-group">
                            <div class="col form-group"><label for="num_ped_compra_fabricacion" class="form-label">Num.
                                    ped. compra</label>
                                <input type="number" min="0" class="form-control" name="num_ped_compra_fabricacion"
                                       id="num_ped_compra_fabricacion"
                                       value="<?php if (isset($operation) && $operation == 'edit' && $fabricacionData) {
                                           echo $fabricacionData['num_ped_compra_fabricacion'];
                                       } ?>"></div>
                            <div class="col form-group"><label for="fecha_ped_compra_fabricacion" class="form-label">Fecha
                                    ped. compra</label>
                                <input type="datetime-local" class="form-control" name="fecha_ped_compra_fabricacion"
                                       id="fecha_ped_compra_fabricacion"
                                       value="<?php if (isset($operation) && $operation == 'edit' && $fabricacionData) {
                                           echo date('Y-m-d', strtotime($fabricacionData['fecha_ped_compra_fabricacion'])) . 'T' . date('H:i', strtotime($fabricacionData['fecha_ped_compra_fabricacion']));
                                       } ?>"></div>
                            <div class="col form-group"><label for="fecha_entrega_fabricacion" class="form-label">Fecha
                                    entrega</label>
                                <input type="datetime-local" class="form-control" name="fecha_entrega_fabricacion"
                                       id="fecha_entrega_fabricacion"
                                       value="<?php if (isset($operation) && $operation == 'edit' && $fabricacionData) {
                                           echo date('Y-m-d', strtotime($fabricacionData['fecha_entrega_fabricacion'])) . 'T' . date('H:i', strtotime($fabricacionData['fecha_entrega_fabricacion']));
                                       } ?>"></div>
                            <div class="col form-group"><label for="operario_fabrica" class="form-label">Operario
                                    fabrica</label>
                                <input type="text" class="form-control" name="operario_fabrica" id="operario_fabrica"
                                       value="<?php if (isset($operation) && $operation == 'edit' && $fabricacionData) {
                                           echo $fabricacionData['operario_fabrica'];
                                       } ?>"></div>
                            <div class="col form-group">
                                <?php
                                $db = getDbInstance();
                                $tallerData = $db->rawQuery("SELECT id_taller,nombre_taller FROM talleres");
                                ?>
                                <label for="id_taller" class="form-label">Taller</label>
                                <select name="id_taller" class="form-control" id="id_taller">
                                    <option value="">Seleccione taller</option>
                                    <?php foreach ($tallerData as $taller): ?>
                                        <option value="<?= $taller['id_taller'] ?>"
                                                <?php if (isset($operation) && $operation == 'edit' && $edit_producto['id_taller'] == $taller['id_taller']): ?>selected <?php endif; ?>><?= $taller['nombre_taller'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade show" id="nav-3e_elem" role="tabpanel" aria-labelledby="3e_elem">
                    <div class="row column-0">
                        <div class="col form-group">
                            <div class="col form-group"><label for="taller_pulimento" class="form-label">Taller</label>
                                <input type="text" class="form-control" name="taller_pulimento" id="taller_pulimento"
                                       value="<?php if (isset($operation) && $operation == 'edit' && $pulimentoData) {
                                           echo $pulimentoData['taller_pulimento'];
                                       } ?>"></div>
                            <div class="col form-group"><label for="num_ped_compra_pulimento" class="form-label">Num.
                                    ped. compra</label>
                                <input type="number" min="0" class="form-control" name="num_ped_compra_pulimento"
                                       id="num_ped_compra_pulimento"
                                       value="<?php if (isset($operation) && $operation == 'edit' && $pulimentoData) {
                                           echo $pulimentoData['num_ped_compra_pulimento'];
                                       } ?>"></div>
                            <div class="col form-group"><label for="fecha_ped_compra_pulimento" class="form-label">Fecha
                                    ped. compra</label>
                                <input type="datetime-local" class="form-control" name="fecha_ped_compra_pulimento"
                                       id="fecha_ped_compra_pulimento"
                                       value="<?php if (isset($operation) && $operation == 'edit' && $pulimentoData) {
                                           echo date('Y-m-d', strtotime($pulimentoData['fecha_ped_compra_pulimento'])) . 'T' . date('H:i', strtotime($pulimentoData['fecha_ped_compra_pulimento']));
                                       } ?>"></div>
                            <div class="col form-group"><label for="fecha_entrega_pulimento" class="form-label">Fecha
                                    entrega</label>
                                <input type="datetime-local" class="form-control" name="fecha_entrega_pulimento"
                                       id="fecha_entrega_pulimento"
                                       value="<?php if (isset($operation) && $operation == 'edit' && $pulimentoData) {
                                           echo date('Y-m-d', strtotime($pulimentoData['fecha_entrega_pulimento'])) . 'T' . date('H:i', strtotime($pulimentoData['fecha_entrega_pulimento']));
                                       } ?>"></div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade show" id="nav-4e_elem" role="tabpanel" aria-labelledby="4e_elem">
                    <div class="row column-0">
                        <div class="col form-group">
                            <div class="col form-group"><label for="taller_complemento"
                                                               class="form-label">Taller</label>
                                <input type="text" class="form-control" name="taller_complemento"
                                       id="taller_complemento"
                                       value="<?php if (isset($operation) && $operation == 'edit' && $complementoData) {
                                           echo $complementoData['taller_complemento'];
                                       } ?>"></div>
                            <div class="col form-group"><label for="num_ped_compra_complemento" class="form-label">Num.
                                    ped. compra</label>
                                <input type="number" min="0" class="form-control" name="num_ped_compra_complemento"
                                       id="num_ped_compra_complemento"
                                       value="<?php if (isset($operation) && $operation == 'edit' && $complementoData) {
                                           echo $complementoData['num_ped_compra_complemento'];
                                       } ?>"></div>
                            <div class="col form-group"><label for="fecha_ped_compra_complemento" class="form-label">Fecha
                                    ped. compra</label>
                                <input type="datetime-local" class="form-control" name="fecha_ped_compra_complemento"
                                       id="fecha_ped_compra_complemento"
                                       value="<?php if (isset($operation) && $operation == 'edit' && $complementoData) {
                                           echo date('Y-m-d', strtotime($complementoData['fecha_ped_compra_complemento'])) . 'T' . date('H:i', strtotime($complementoData['fecha_ped_compra_complemento']));
                                       } ?>"></div>
                            <div class="col form-group">
                                <label for="fecha_entrega_complemento" class="form-label">Fecha entrega</label>
                                <input type="datetime-local" class="form-control" name="fecha_entrega_complemento"
                                       id="fecha_entrega_complemento"
                                       value="<?php if (isset($operation) && $operation == 'edit' && $complementoData) {
                                           echo date('Y-m-d', strtotime($complementoData['fecha_entrega_complemento'])) . 'T' . date('H:i', strtotime($complementoData['fecha_entrega_complemento']));
                                       } ?>"></div>
                        </div>
                    </div>
                </div>

                </div>
                <div class="col-12">
                    <button type="submit" class="btn btn-primary">Confirmar</button>
                </div>
            </div>
        </div>
    </div>
</div>
