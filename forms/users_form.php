<!-- Text input -->
<div class="form-group">
    <label class="col-md-4 control-label">Usuario</label>
    <div class="col-md-4 inputGroupContainer">
        <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
            <input type="text" name="user_name" placeholder="Usuario" class="form-control" required=""
                   value="<?php echo ($edit) ? $admin_account['user_name'] : ''; ?>" autocomplete="off">
        </div>
    </div>
</div>
<!-- Password input -->
<div class="form-group">
    <label class="col-md-4 control-label">Contraseña</label>
    <div class="col-md-4 inputGroupContainer">
        <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
            <input type="password" name="password" placeholder="Contraseña" class="form-control" required=""
                   autocomplete="off">
        </div>
    </div>
</div>
<!-- Text input -->
<div class="form-group">
    <label class="col-md-4 control-label">Email</label>
    <div class="col-md-4 inputGroupContainer">
        <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
            <input type="email" name="email" value="<?php echo ($edit) ? $admin_account['email'] : ''; ?>" placeholder="Email" class="form-control" autocomplete="off">
        </div>
    </div>
</div>
<!-- Textarea input -->
<div class="form-group">
    <label class="col-md-4 control-label">Descripcion</label>
    <div class="col-md-4 inputGroupContainer">
        <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
            <textarea name="descripcion" placeholder="Descripcion" class="form-control"><?php echo ($edit) ? $admin_account['descripcion'] : ''; ?></textarea>
        </div>
    </div>
</div>
<input type="hidden" name="user_type" value="trabajador">
<!-- Submit button -->
<div class="form-group">
    <label class="col-md-4 control-label"></label>
    <div class="col-md-4">
        <button type="submit" class="btn btn-warning">Guardar <i class="glyphicon glyphicon-send"></i></button>
    </div>
</div>
