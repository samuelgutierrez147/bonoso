<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.css">
<script src="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.js"></script>


<div class="container">
    <form class="well form-horizontal" action="" method="post" id="contact_form" enctype="multipart/form-data">
        <div class="col">
            <div class="col-lg-6">
                <label for="obra" class="obra">Obra</label>
                <input type="text" class="form-control" id="obra" name="obra"
                       value="<?php if (isset($operation) && $operation == 'edit') {
                           echo $edit_obra['obra'];
                       } ?>">
            </div>

            <div class="col-lg-6">
                <?php
                $productoData = $db->rawQuery("SELECT id_producto, producto, num_pedido_cliente FROM producto");
                $obraIdProductos = [];
                if (isset($operation) && $operation == 'edit') {
                    if(!is_null($edit_obra['productos_obra']) || $edit_obra['productos_obra'] != '')
                        $obraIdProductos = explode(',',$edit_obra['productos_obra']);
                }
                ?>
                <label for="productos_obra" class="form-label">Productos</label>

                <select id="productos_obra" name="productos_obra[]" placeholder="Seleccione los productos" multiple>
                    <?php foreach ($productoData as $producto): ?>
                        <option value="<?= $producto['id_producto'] ?>" <?php if (in_array($producto['id_producto'], $obraIdProductos)): echo 'selected'; endif;?>>
                            <?=  $producto['producto'] . ' (' . $producto['num_pedido_cliente'] . ')' ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="col-lg-6">
                <label for="tipo_trabajo" class="form-label">Tipo de trabajo</label>
                <select name="tipo_trabajo" class="form-control" id="tipo_trabajo">
                    <option value="fabricacion"<?php if (isset($operation) && $operation == 'edit' && $edit_obra['tipo_trabajo'] == 'fabricacion') {
                        echo ' selected';
                    } ?>>fabricación
                    </option>
                    <option value="pulimento"<?php if (isset($operation) && $operation == 'edit' && $edit_obra['tipo_trabajo'] == 'pulimento') {
                        echo ' selected';
                    } ?>>pulimento
                    </option>
                    <option value="taller complemento"<?php if (isset($operation) && $operation == 'edit' && $edit_obra['tipo_trabajo'] == 'taller_complemento') {
                        echo ' selected';
                    } ?>>taller complemento
                    </option>
                    <option value="terminado"<?php if (isset($operation) && $operation == 'edit' && $edit_obra['tipo_trabajo'] == 'terminado') {
                        echo ' selected';
                    } ?>>terminado
                    </option>
                </select>
            </div>

            <div class="col-lg-6">
                <label for="hora_inicio_obra" class="form-label">Hora inicio</label>
                <input type="datetime-local" class="form-control" id="hora_inicio_obra" name="hora_inicio_obra"
                       value="<?php if (isset($operation) && $operation == 'edit') {
                           echo date('Y-m-d', strtotime($edit_obra['hora_inicio_obra'])) . 'T' . date('H:i', strtotime($edit_obra['hora_inicio_obra']));
                       } ?>">
            </div>
            <div class="col-lg-6">
                <label for="hora_fin_obra" class="form-label">Hora fin</label>
                <input type="datetime-local" class="form-control" id="hora_fin_obra" name="hora_fin_obra"
                       value="<?php if (isset($operation) && $operation == 'edit') {
                           echo date('Y-m-d', strtotime($edit_obra['hora_fin_obra'])) . 'T' . date('H:i', strtotime($edit_obra['hora_fin_obra']));
                       } ?>">
            </div>
            <div class="col-lg-6">
                <label for="observaciones_obra" class="form-label">Observaciones</label>
                <input type="text" class="form-control" id="observaciones_obra" name="observaciones_obra"
                       value="<?php if (isset($operation) && $operation == 'edit') {
                           echo $edit_obra['observaciones_obra'];
                       } ?>">
            </div>
            <div class="col-lg-6">
                <label for="horas_estimadas" class="form-label">Horas estimadas</label>
                <input type="number" class="form-control" id="horas_estimadas" name="horas_estimadas"
                       value="<?php if (isset($operation) && $operation == 'edit') {
                           echo $edit_obra['horas_estimadas'];
                       } ?>">
            </div>
            <?php if($_SESSION['is_admin']): ?>
                <br>
                <div class="col-lg-6">
                    <input type="checkbox" name="show_obra" id="show_obra" <?php if (isset($operation) && $operation == 'edit' && $edit_obra['show_obra']): echo 'checked'; endif; ?>>
                    <label for="show_obra" class="form-label">Mostrar obra</label>
                </div>
            <?php endif; ?>
            <br>
            <div class="col-lg-12">
                <button type="submit" class="btn btn-primary">Confirmar</button>
            </div>

        </div>
    </form>
</div>
<script>
    var multipleCancelButton = new Choices('#productos_obra', {
        removeItemButton: true,
        maxItemCount: 20,
        searchResultLimit: 20,
        renderChoiceLimit: 20
    });

    $('.choices__inner').attr('style', 'background-color: white !important; border-radius: 5px');
    $('.choices__input.choices__input--cloned').attr('style', 'background-color: white !important');
</script>