<!-- Text input -->
<div class="form-group">
    <label class="col-md-4 control-label">Valor</label>
    <div class="col-md-4 inputGroupContainer">
        <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
            <input type="text" name="value" placeholder="Valor" class="form-control" required=""
                   value="<?php echo ($edit) ? $configData['value'] : ''; ?>" autocomplete="off">
        </div>
    </div>
</div>
<input type="hidden" name="path" value="<?php echo ($edit) ? $configData['path'] : ''; ?>">
<input type="hidden" name="name" value="<?php echo ($edit) ? $configData['name'] : ''; ?>">
<!-- Submit button -->
<div class="form-group">
    <label class="col-md-4 control-label"></label>
    <div class="col-md-4">
        <button type="submit" class="btn btn-warning">Guardar <i class="glyphicon glyphicon-send"></i></button>
    </div>
</div>
