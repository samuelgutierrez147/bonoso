<?php
session_start();
require_once 'config/config.php';
require_once BASE_PATH . '/includes/auth_validate.php';

// Users class
require_once BASE_PATH . '/lib/General/Producto.php';
$producto = new Producto();
$edit = false;
$db = getDbInstance();

// Serve POST request
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    // Sanitize input post if we want
    $data_to_db = filterDataPost();

    if ($data_to_db['estado'] == '') {
        unset($data_to_db['estado']);
    }
    $idTaller = $data_to_db['id_taller'];
    $fechaPedCompraFabricacion = $data_to_db['fecha_ped_compra_fabricacion'];
    $horasFabrica = $data_to_db['horas_fabrica'];

    $horasProducto = [
        'horas_fabrica' => $data_to_db['horas_fabrica'],
        'horas_pulimento' => $data_to_db['horas_pulimento'],
        'horas_mont_ext' => $data_to_db['horas_mont_ext'],
        'horas_fabricacion_y_pulimento' => $data_to_db['horas_fabricacion_y_pulimento']];

    unset($data_to_db['horas_fabrica']);
    unset($data_to_db['horas_pulimento']);
    unset($data_to_db['horas_mont_ext']);
    unset($data_to_db['horas_fabricacion_y_pulimento']);

    if (!empty(array_filter($horasProducto))) {
        $horasInsert = $db->insert('horas_produccion', $horasProducto);
        if ($horasInsert) {
            $data_to_db['id_horas'] = $horasInsert;
        }
    }

    $fabricacionProducto = [
        'num_ped_compra_fabricacion' => $data_to_db['num_ped_compra_fabricacion'],
        'fecha_ped_compra_fabricacion' => date("Y-m-d H:i:s", strtotime($data_to_db['fecha_ped_compra_fabricacion'])),
        'fecha_entrega_fabricacion' => date("Y-m-d H:i:s", strtotime($data_to_db['fecha_entrega_fabricacion'])),
        'operario_fabrica' => $data_to_db['operario_fabrica']];

    unset($data_to_db['num_ped_compra_fabricacion']);
    unset($data_to_db['fecha_ped_compra_fabricacion']);
    unset($data_to_db['fecha_entrega_fabricacion']);
    unset($data_to_db['operario_fabrica']);

    if (!empty(array_filter($fabricacionProducto))) {
        $fabricacionInsert = $db->insert('fabricacion', $fabricacionProducto);
        if ($fabricacionInsert) {
            $data_to_db['id_fabricacion'] = $fabricacionInsert;
        }
    }

    $pulimentoProducto = [
        'num_ped_compra_pulimento' => $data_to_db['num_ped_compra_pulimento'],
        'taller_pulimento' => $data_to_db['taller_pulimento'],
        'fecha_ped_compra_pulimento' => date("Y-m-d H:i:s", strtotime($data_to_db['fecha_ped_compra_pulimento'])),
        'fecha_entrega_pulimento' => date("Y-m-d H:i:s", strtotime($data_to_db['fecha_entrega_pulimento']))
    ];

    unset($data_to_db['num_ped_compra_pulimento']);
    unset($data_to_db['taller_pulimento']);
    unset($data_to_db['fecha_ped_compra_pulimento']);
    unset($data_to_db['fecha_entrega_pulimento']);

    if (!empty(array_filter($pulimentoProducto))) {
        $pulimentoInsert = $db->insert('pulimento', $pulimentoProducto);
        if ($pulimentoInsert) {
            $data_to_db['id_pulimento'] = $pulimentoInsert;
        }
    }

    $complementoProducto = [
        'num_ped_compra_complemento' => $data_to_db['num_ped_compra_complemento'],
        'taller_complemento' => $data_to_db['taller_complemento'],
        'fecha_ped_compra_complemento' => date("Y-m-d H:i:s", strtotime($data_to_db['fecha_ped_compra_complemento'])),
        'fecha_entrega_complemento' => date("Y-m-d H:i:s", strtotime($data_to_db['fecha_entrega_complemento']))
    ];

    unset($data_to_db['num_ped_compra_complemento']);
    unset($data_to_db['taller_complemento']);
    unset($data_to_db['fecha_ped_compra_complemento']);
    unset($data_to_db['fecha_entrega_complemento']);

    if (!empty(array_filter($complementoProducto))) {
        $complementoInsert = $db->insert('taller_complemento', $complementoProducto);
        if ($complementoInsert) {
            $data_to_db['id_taller_complemento'] = $complementoInsert;
        }
    }

    $last_id = $db->insert('producto', $data_to_db);
    if ($last_id) {
        $_SESSION['success'] = 'Creado correctamente';
    } else {
        $_SESSION['failure'] = 'Error al crear el producto: ' . $db->getLastError();
    }
    header('location: producto.php');
    exit;

}
?>


<?php include BASE_PATH . '/includes/header.php'; ?>
<div class="container" style="margin-top: 15px;">
    <div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">Añadir</h2>
        </div>
    </div>
    <?php include BASE_PATH . '/includes/flash_messages.php'; ?>
    <form class="well form-horizontal" action="" method="post" id="contact_form" enctype="multipart/form-data">
        <?php include BASE_PATH . '/forms/edit_product_form.php'; ?>
    </form>

    <?php include BASE_PATH . '/includes/footer.php'; ?>

