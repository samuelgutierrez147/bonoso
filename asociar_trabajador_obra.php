<?php
session_start();
require_once 'includes/auth_validate.php';
require_once './config/config.php';
$trabajadores = $_POST['trabajadores'];
$id_obra = filter_input(INPUT_POST, 'id_obra');
$db = getDbInstance();

if (!empty($trabajadores) && $id_obra && $_SERVER['REQUEST_METHOD'] == 'POST') {

    $db->where('id_obra', $id_obra);
    $implode= implode(",",$trabajadores);

    $stat = $db->update('obra', ['id_users' => $implode]);
    if ($stat) {
        $_SESSION['success'] = "Obra modificado correctamente";
        header('location:obra.php');
        exit;
    }
}
