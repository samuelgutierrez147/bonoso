// Call the dataTables jQuery plugin
$(document).ready(function() {
  $('#dataTable').DataTable(
      {
        "language": {
          "info": "Página _PAGE_ de _PAGES_",
          "search": "Buscar",
          "lengthMenu": "Mostrar _MENU_ registros",
          "infoEmpty": "Sin registros",
          "paginate": {
              "previous": "Primero",
              "next": "Último"
          },
          "infoFiltered": " - filtrado en _MAX_ registros",
          "zeroRecords": "No se encontraron resultados al filtrar"
        }
      }
  )
});
