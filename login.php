<?php
session_start();
require_once 'config/config.php';
$token = bin2hex(openssl_random_pseudo_bytes(16));

// If User has already logged in, redirect to dashboard page.
if (isset($_SESSION['user_logged_in']) && $_SESSION['user_logged_in'] === TRUE) {
    header('Location:inicio.php');
}

// If user has previously selected "remember me option": 
if (isset($_COOKIE['series_id']) && isset($_COOKIE['remember_token'])) {
    // Get user credentials from cookies.
    $series_id = filter_var($_COOKIE['series_id']);
    $remember_token = filter_var($_COOKIE['remember_token']);
    $db = getDbInstance();
    // Get user By series ID:
    $db->where('series_id', $series_id);
    try {
        $row = $db->getOne('users');

        if ($db->count >= 1) {
            // User found. verify remember token
            if (password_verify($remember_token, $row['remember_token'])) {
                // Verify if expiry time is modified.
                $expires = strtotime($row['expires']);

                if (strtotime(date('Y-m-d H:i:s')) > $expires) {
                    // Remember Cookie has expired.
                    clearAuthCookie();
                    header('Location: login.php');
                    exit;
                }

                $_SESSION['user_logged_in'] = TRUE;
                //get if is admin
                $_SESSION['is_admin'] = ($row['user_type'] == 'admin') ?? false;
                header('Location:inicio.php');
                exit;
            } else {
                clearAuthCookie();
                header('Location: login.php');
                exit;
            }
        } else {
            clearAuthCookie();
            header('Location: login.php');
            exit;
        }
    } catch (Exception $e) {
        echo $e->getCode() . " " . $e->getMessage();
    }

}
?>
<?php include BASE_PATH . '/includes/header.php'; ?>
<body class="bg-gradient-primary">

<div class="container">
    <!-- Outer Row -->
    <div class="row justify-content-center">
        <div class="col-xl-10 col-lg-12 col-md-9">
            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg-6 d-none d-lg-block bg-register-image"></div>
                        <div class="col-lg-6">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">Acceso</h1>
                                </div>
                                <form class="user" method="POST" action="authenticate.php">
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control-user"
                                               id="username" required name="username" aria-describedby="emailHelp"
                                               placeholder="Usuario">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control form-control-user"
                                               id="password" required name="password" placeholder="Contraseña">
                                    </div>
                                    <div class="form-group">
                                        <div class="custom-control custom-checkbox small">
                                            <input type="checkbox" name="remember" class="custom-control-input"
                                                   id="remember">
                                            <label class="custom-control-label" for="remember">Recordar</label>
                                        </div>
                                    </div>
                                    <?php if (isset($_SESSION['login_failure'])): ?>
                                        <div class="alert alert-danger" role="alert">
                                            <a href="#" class="close" data-dismiss="alert"
                                               aria-label="close">&times;</a>
                                            <?php
                                            echo $_SESSION['login_failure'];
                                            unset($_SESSION['login_failure']);
                                            ?>
                                        </div>
                                    <?php endif; ?>
                                    <button type="submit" class="btn btn-primary btn-user btn-block">Acceder</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include BASE_PATH . '/includes/footer.php'; ?>
