<?php
session_start();
require_once 'config/config.php';
require_once BASE_PATH . '/includes/auth_validate.php';
require_once BASE_PATH . '/lib/Users/Users.php';
$users = new Users();
// current page
$page = filter_input(INPUT_GET, 'page');
if (!$page) {
    $page = 1;
}

// Get DB instance. i.e instance of MYSQLiDB Library
$db = getDbInstance();
$select = array('id_user', 'user_name', 'user_type', 'permisos');

// Get result of the query
$rows = $db->get('users');
?>
<?php include BASE_PATH . '/includes/header.php'; ?>
<!-- Main container -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Usuarios</h1>
        <a href="add_user.php" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                    class="fas fa-plus fa-sm text-white-50"></i> Añadir nuevo</a>
    </div>
    <?php include BASE_PATH . '/includes/flash_messages.php'; ?>

    <!-- Table -->
    <table class="table table-bordered" id="dataTable">
        <thead>
        <tr>
            <th width="45%">Usuario</th>
            <th width="10%">Acciones</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($rows as $row): ?>
            <tr>
                <td><?php echo htmlspecialchars($row['user_name']); ?></td>
                <td>
                    <a href="permisos.php?id_user=<?php echo $row['id_user']; ?>"
                       class="btn btn-info btn-sm">
                        <i class="fas fa-key" aria-hidden="true"></i>
                    </a>
                    <a href="edit_user.php?id_user=<?php echo $row['id_user']; ?>&operation=edit"
                       class="btn btn-info btn-sm">
                        <i class="fas fa-edit" aria-hidden="true"></i>
                    </a>
                    <a href="#" data-toggle="modal" data-target="#confirm-delete-<?php echo $row['id_user']; ?>"
                       class="btn btn-info btn-sm">
                        <i class="fas fa-trash" aria-hidden="true"></i>
                    </a>
                </td>
            </tr>
            <!-- Delete Confirmation Modal -->
            <?= generatePopUpDelete($row['id_user'], 'usuario', 'delete_user.php') ?>
            <!-- //Delete Confirmation Modal -->
        <?php endforeach; ?>
        </tbody>
    </table>
    <!-- //Table -->
</div>
<!-- //Main container -->
<?php include BASE_PATH . '/includes/footer.php'; ?>
