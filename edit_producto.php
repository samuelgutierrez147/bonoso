<?php
//Inicio sesion
session_start();
//Acceso a la configuracion
require_once 'config/config.php';
//Clase validadora
require_once BASE_PATH . '/includes/auth_validate.php';

// producto class
require_once BASE_PATH . '/lib/General/Producto.php';
$producto = new Producto();

//Llamos a un valor externo, el get se recoge a traves de la url PARAMETROS DE LA URL
$id_producto = filter_input(INPUT_GET, 'id_producto', FILTER_VALIDATE_INT);
$operation = filter_input(INPUT_GET, 'operation', FILTER_SANITIZE_STRING);

($operation == 'edit') ? $edit = true : $edit = false;

// Serve POST request
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    // Sanitize input post if we want
    $data_to_db = filterDataPost();

    $db = getDbInstance();
    $db->where('id_producto', $id_producto);
    $productFicha = $db->getOne("producto");
    $idTaller = $data_to_db['id_taller'];
    $fechaPedCompraFabricacion = $data_to_db['fecha_ped_compra_fabricacion'];
    $horasFabrica = $data_to_db['horas_fabrica'];

    $horasProducto = [
        'horas_fabrica' => $data_to_db['horas_fabrica'],
        'horas_pulimento' => $data_to_db['horas_pulimento'],
        'horas_mont_ext' => $data_to_db['horas_mont_ext'],
        'horas_fabricacion_y_pulimento' => $data_to_db['horas_fabricacion_y_pulimento']];

    unset($data_to_db['horas_fabrica']);
    unset($data_to_db['horas_pulimento']);
    unset($data_to_db['horas_mont_ext']);
    unset($data_to_db['horas_fabricacion_y_pulimento']);

    if (is_null($productFicha['id_horas'])) {
        $horasInsert = $db->insert('horas_produccion', $horasProducto);
        if ($horasInsert) {
            $data_to_db['id_horas'] = $horasInsert;
        }
    } else {
        $db->where('id_horas', $productFicha['id_horas']);
        $db->update("horas_produccion", $horasProducto);
    }

    $fabricacionProducto = [
        'num_ped_compra_fabricacion' => $data_to_db['num_ped_compra_fabricacion'],
        'fecha_ped_compra_fabricacion' => date("Y-m-d H:i:s", strtotime($data_to_db['fecha_ped_compra_fabricacion'])),
        'fecha_entrega_fabricacion' => date("Y-m-d H:i:s", strtotime($data_to_db['fecha_entrega_fabricacion'])),
        'operario_fabrica' => $data_to_db['operario_fabrica']];

    unset($data_to_db['num_ped_compra_fabricacion']);
    unset($data_to_db['fecha_ped_compra_fabricacion']);
    unset($data_to_db['fecha_entrega_fabricacion']);
    unset($data_to_db['operario_fabrica']);

    if (is_null($productFicha['id_fabricacion'])) {
        $fabricacionInsert = $db->insert('fabricacion', $fabricacionProducto);
        if ($fabricacionInsert) {
            $data_to_db['id_fabricacion'] = $fabricacionInsert;
        }
    } else {
        $db->where('id_fabricacion', $productFicha['id_fabricacion']);
        if ($data_to_db['estado'] == 'terminado')
            $fabricacionProducto['fecha_terminado'] = date('Y-m-d H:i:s');
        else
            $fabricacionProducto['fecha_terminado'] = NULL;
        
        $db->update("fabricacion", $fabricacionProducto);
    }

    $pulimentoProducto = [
        'num_ped_compra_pulimento' => $data_to_db['num_ped_compra_pulimento'],
        'taller_pulimento' => $data_to_db['taller_pulimento'],
        'fecha_ped_compra_pulimento' => date("Y-m-d H:i:s", strtotime($data_to_db['fecha_ped_compra_pulimento'])),
        'fecha_entrega_pulimento' => date("Y-m-d H:i:s", strtotime($data_to_db['fecha_entrega_pulimento']))
    ];


    unset($data_to_db['num_ped_compra_pulimento']);
    unset($data_to_db['taller_pulimento']);
    unset($data_to_db['fecha_ped_compra_pulimento']);
    unset($data_to_db['fecha_entrega_pulimento']);

    if (is_null($productFicha['id_pulimento'])) {
        $pulimentoInsert = $db->insert('pulimento', $pulimentoProducto);
        if ($pulimentoInsert) {
            $data_to_db['id_pulimento'] = $pulimentoInsert;
        }
    } else {
        $db->where('id_pulimento', $productFicha['id_pulimento']);
        $db->update("pulimento", $pulimentoProducto);
    }

    $complementoProducto = [
        'num_ped_compra_complemento' => $data_to_db['num_ped_compra_complemento'],
        'taller_complemento' => $data_to_db['taller_complemento'],
        'fecha_ped_compra_complemento' => date("Y-m-d H:i:s", strtotime($data_to_db['fecha_ped_compra_complemento'])),
        'fecha_entrega_complemento' => date("Y-m-d H:i:s", strtotime($data_to_db['fecha_entrega_complemento']))
    ];

    unset($data_to_db['num_ped_compra_complemento']);
    unset($data_to_db['taller_complemento']);
    unset($data_to_db['fecha_ped_compra_complemento']);
    unset($data_to_db['fecha_entrega_complemento']);

    if (is_null($productFicha['id_taller_complemento'])) {
        $complementoInsert = $db->insert('taller_complemento', $complementoProducto);
        if ($complementoInsert) {
            $data_to_db['id_taller_complemento'] = $complementoInsert;
        }
    } else {
        $db->where('id_taller_complemento', $productFicha['id_taller_complemento']);
        $db->update("taller_complemento", $complementoProducto);
    }

    $db->where('id_producto', $id_producto, '=');
    $stat = $db->update('producto', $data_to_db);
    if ($stat) {
        $_SESSION['success'] = 'Producto<b> con el ID ' . $id_producto . '</b> actualizado';
    } else {
        $_SESSION['failure'] = 'Algo falló al actualizar el producto: ' . $db->getLastError();
    }
    header('location: edit_producto.php?id_producto=' . $id_producto . '&operation=edit');
    exit;
}


$db = getDbInstance();
$db->where('id_producto', $id_producto);
$edit_producto = $db->getOne("producto");

$horasData = [];
if ($edit_producto['id_horas']) {
    $db->where('id_horas', $edit_producto['id_horas']);
    $horasData = $db->getOne("horas_produccion");
}

$fabricacionData = [];
if ($edit_producto['id_fabricacion']) {
    $db->where('id_fabricacion', $edit_producto['id_fabricacion']);
    $fabricacionData = $db->getOne("fabricacion");
}

$pulimentoData = [];
if ($edit_producto['id_pulimento']) {
    $db->where('id_pulimento', $edit_producto['id_pulimento']);
    $pulimentoData = $db->getOne("pulimento");
}

$complementoData = [];
if ($edit_producto['id_taller_complemento']) {
    $db->where('id_taller_complemento', $edit_producto['id_taller_complemento']);
    $complementoData = $db->getOne("taller_complemento");
}


?>
<?php include BASE_PATH . '/includes/header.php'; ?>
<!-- Begin Page Content -->
<div class="container-fluid">
    <?php include BASE_PATH . '/includes/flash_messages.php'; ?>
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Editar</h1>
    </div>

    <form class="well form-horizontal" action="" method="post" id="contact_form" enctype="multipart/form-data">
        <?php include BASE_PATH . '/forms/edit_product_form.php'; ?>
    </form>
</div>
<?php include BASE_PATH . '/includes/footer.php'; ?>
