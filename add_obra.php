<?php
session_start();
require_once 'config/config.php';
require_once BASE_PATH . '/includes/auth_validate.php';

// Users class
require_once BASE_PATH . '/lib/General/Producto.php';
$obra = new Producto();
$edit = false;
$db = getDbInstance();

// Serve POST request
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    // Sanitize input post if we want
    $data_to_db = filterDataPost();
    if(!isset($data_to_db['show_obra'])){
        $data_to_db['show_obra'] = 0;
    }

    $data_to_db['productos_obra'] = implode(',', $data_to_db['productos_obra']);

    try {
        $last_id = $db->insert('obra', $data_to_db);
        if ($last_id) {
            $_SESSION['success'] = 'Creado correctamente';
        }
    } catch (Exception $e) {
        $_SESSION['failure'] = $e->getMessage();
    }
    header('location: obra.php');
    exit;
}
?>


<?php include BASE_PATH . '/includes/header.php'; ?>
<div class="container" style="margin-top: 15px;">
    <div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">Añadir</h2>
        </div>
    </div>
    <?php include BASE_PATH . '/includes/flash_messages.php'; ?>
    <form class="well form-horizontal" action="" method="post" id="contact_form" enctype="multipart/form-data">
        <?php include BASE_PATH . '/forms/obra_form.php'; ?>
    </form>

    <?php include BASE_PATH . '/includes/footer.php'; ?>
