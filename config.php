<?php
session_start();

if (!$_SESSION['is_admin']) {
    $_SESSION['failure'] = 'No tiene acceso';
    header('location: index.php');
    exit;
}
require_once 'config/config.php';
// Get current page
$page = filter_input(INPUT_GET, 'page');
if (!$page) {
    $page = 1;
}

// Get DB instance. i.e instance of MYSQLiDB Library
$db = getDbInstance();
$select = array('config_id', 'value', 'name');

// Get result of the query
$configList = $db->arraybuilder()->paginate('config_data', $page, $select);
$total_pages = $db->totalPages;
// Get current page
$page = filter_input(INPUT_GET, 'page');
if (!$page) {
    $page = 1;
}
?>
<?php include BASE_PATH . '/includes/header.php'; ?>
<!-- Main container -->
<div class="container-fluid">
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Configuraciones app</h1>
    </div>
    <?php include BASE_PATH . '/includes/flash_messages.php'; ?>

    <!-- Table -->
    <table class="table table-bordered" id="dataTable">
        <thead>
        <tr>
            <th>Name</th>
            <th>Valor</th>
            <th width="10%">Acciones</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($configList as $row): ?>
            <tr>
                <td><?php echo htmlspecialchars($row['name']); ?></td>
                <td><?php echo htmlspecialchars($row['value']); ?></td>
                <td>
                    <a href="edit_config.php?config_id=<?php echo $row['config_id']; ?>&operation=edit"
                       class="btn btn-info btn-sm">
                        <i class="fas fa-edit" aria-hidden="true"></i>
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <!-- //Table -->
</div>
<!-- //Main container -->
<?php include BASE_PATH . '/includes/footer.php'; ?>
