<?php
session_start();
require_once 'includes/auth_validate.php';
require_once './config/config.php';
$del_id = filter_input(INPUT_POST, 'del_id');
$db = getDbInstance();

if(!$_SESSION['is_admin']){
    header('HTTP/1.1 401 Unauthorized', true, 401);
    exit("401 Unauthorized");
}


// Delete a user using id_user
if ($del_id && $_SERVER['REQUEST_METHOD'] == 'POST') {
    $db->where('id_obra', $del_id);
    $stat = $db->delete('obra');
    if ($stat) {
        $_SESSION['success'] = "Obra borrada correctamente";
        header('location:obra.php');
        exit;
    }
}