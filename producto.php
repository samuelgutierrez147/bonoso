<?php
session_start();
require_once 'config/config.php';
require_once BASE_PATH . '/includes/auth_validate.php';
require_once BASE_PATH . '/lib/General/Producto.php';
$producto = new Producto();
// Get current page
$page = filter_input(INPUT_GET, 'page');
if (!$page) {
    $page = 1;
}

// Get DB instance. i.e instance of MYSQLiDB Library
$db = getDbInstance();
// Get result of the query
$rows = $db->query('SELECT * FROM producto');

?>
<?php include BASE_PATH . '/includes/header.php'; ?>
<!-- Main container -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Producto</h1>
        <a href="add_obra.php" class="d-none d-sm-inline-block btn btn-sm btn-dark shadow-sm"><i
                    class="fas fa-plus fa-sm text-white-50"></i> Crear obra</a>
        <a href="add_producto.php" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                    class="fas fa-plus fa-sm text-white-50"></i> Añadir nuevo</a>
    </div>
    <?php include BASE_PATH . '/includes/flash_messages.php'; ?>

    <!-- Table -->
    <table class="table table-bordered table-scroll" id="table-producto">
        <thead>
        <tr>
            <th>Num. pedido</th>
            <th>Fecha pedido</th>
            <th>Cliente</th>
            <th>Referencia</th>
            <th>Nº unidades</th>
            <th>Referencia art.</th>
            <th>Articulo</th>
            <th>Horas de montaje</th>
            <th>Taller</th>
            <th>Taller Pulimento</th>
            <th>Taller Complemento</th>
            <th>Gestor</th>
            <th>Estado</th>
            <th>Acciones</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($rows as $row):
            $horasData = [];
            if(!is_null($row['id_horas'])){
                $horasData = $db->rawQuery("SELECT * FROM horas_produccion WHERE id_horas = {$row['id_horas']}");
                $horasData = reset($horasData);
            }

            ?>
            <tr>
                <td>
                    <?php echo htmlspecialchars($row['num_pedido_cliente']); ?>
                </td>
                <td>
                    <span style="display: none"><?= strtotime($row['fecha_ped_cliente']) ?></span> <?php echo htmlspecialchars($row['fecha_ped_cliente']); ?>
                </td>
                <td>
                    <?php echo htmlspecialchars($row['cliente']); ?>
                </td>
                <td>
                    <?php echo htmlspecialchars($row['referencia']); ?>
                </td>
                <td>
                    <?php echo htmlspecialchars($row['num_unidad']); ?>
                </td>
                <td>
                    <?php echo htmlspecialchars($row['ref_articulo']); ?>
                </td>
                <td>
                    <a href="#" data-toggle="modal"
                       data-target="#product-<?php echo $row['id_producto']; ?>"><?php echo $row['producto']; ?>
                    </a>
                </td>
                <td>
                    <?php echo htmlspecialchars($horasData['horas_mont_ext'] ?? ''); ?>
                </td>
                <td>
                    <?php echo htmlspecialchars($producto->getNameByTallerId($row['id_taller'])); ?>
                </td>
                <td>
                    <a href="#" data-toggle="modal"
                       data-target="#pulimento-<?php echo $row['id_pulimento']; ?>"><?php echo $nombrepulimento = $producto->getNameByPulimentoId($row['id_pulimento']) ?></a>
                </td>
                <td>
                    <a href="#" data-toggle="modal"
                       data-target="#complemento-<?php echo $row['id_taller_complemento']; ?>"><?php echo $nombrecomplemento = $producto->getNameByComplementoId($row['id_taller_complemento']) ?></a>

                </td>
                <td>
                    <?php echo htmlspecialchars($row['gestor']); ?>
                </td>
                <td><?php echo htmlspecialchars($row['estado']); ?></td>
                <td>
                    <?php if ($row['estado'] == "pendiente subcont") : ?>
                        <a href="#" data-toggle="modal" data-target="#talleres_user-<?php echo $row['id_producto']; ?>"
                           class="btn btn-info btn-sm">
                            <i class="fas fa-industry" aria-hidden="true"></i>
                        </a>
                    <?php else: ?>
                        <a href="edit_producto.php?id_producto=<?php echo $row['id_producto']; ?>&operation=edit"
                           class="btn btn-info btn-sm">
                            <i class="fas fa-edit" aria-hidden="true"></i>
                        </a>
                    <?php endif; ?>
                    <a href="#" data-toggle="modal" data-target="#confirm-delete-<?php echo $row['id_producto']; ?>"
                       class="btn btn-info btn-sm">
                        <i class="fas fa-trash" aria-hidden="true"></i>
                    </a>
                </td>
            </tr>
            <!-- Delete Confirmation Modal -->
            <?= generatePopUpDelete($row['id_producto'], 'producto', 'delete_producto.php') ?>
            <?php
            $productData = $db->rawQuery("SELECT prioridad,num_pedido_cliente, cliente,
            referencia, asunto, num_unidad, ref_articulo, observaciones FROM producto WHERE id_producto = {$row['id_producto']}");
            $productData = reset($productData);
            ?>
            <!-- popup producto -->
            <div class="modal fade" id="product-<?php echo $row['id_producto']; ?>" tabindex="-1" role="dialog"
                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Información del producto</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <?php foreach ($productData as $key => $product) : ?>
                                <b><?= $key ?></b> - <?= $product ?><br>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>


            <div class="modal fade" id="horas-<?php echo $row['id_horas']; ?>" tabindex="-1" role="dialog"
                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Información del producto</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <?php if ($horasData):
                                foreach ($horasData as $key => $horas) : ?>
                                    <b><?= $key ?></b> - <?= $horas ?><br>
                                <?php endforeach;
                            else: ?>
                                <b>No tiene horas asociadas</b>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>

            <!-- popup pulimento -->
            <?php
            $pulimentoData = $db->rawQuery("SELECT num_ped_compra_pulimento, fecha_ped_compra_pulimento,
            fecha_entrega_pulimento FROM pulimento");
            $pulimentoData = reset($pulimentoData);
            ?>
            <div class="modal fade" id="pulimento-<?php echo $row['id_pulimento']; ?>" tabindex="-1" role="dialog"
                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Información del producto</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <?php if ($pulimentoData):
                                foreach ($pulimentoData as $key => $pulimento) : ?>
                                    <b><?= $key ?></b> - <?= $pulimento ?><br>
                                <?php endforeach;
                            else: ?>
                                <b>No tiene pulimento asignado</b>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>

            <!-- popup taller complemento -->

            <?php
            $complementoData = $db->rawQuery("SELECT num_ped_compra_complemento, fecha_ped_compra_complemento,
            fecha_entrega_complemento FROM taller_complemento");
            $complementoData = reset($complementoData);
            ?>
            <div class="modal fade" id="complemento-<?php echo $row['id_taller_complemento']; ?>" tabindex="-1"
                 role="dialog"
                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Información del producto</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <?php if ($complementoData):
                                foreach ($complementoData as $key => $complemento) : ?>
                                    <b><?= $key ?></b> - <?= $complemento ?><br>
                                <?php endforeach;
                            else: ?>
                                <b>No tiene taller complemento asignado</b>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>

            <?php
            $productData = $db->rawQuery("SELECT num_pedido_cliente, cliente,
            referencia, asunto, num_unidad, ref_articulo, observaciones FROM producto WHERE id_producto = {$row['id_producto']}");
            $productData = reset($productData);
            ?>
            <div class="modal fade" id="product-<?php echo $row['id_producto']; ?>" tabindex="-1" role="dialog"
                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Información del producto</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <?php foreach ($productData as $key => $product) : ?>
                                <b><?= $key ?></b> - <?= $product ?><br>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="talleres_user-<?php echo $row['id_producto']; ?>" tabindex="-1" role="dialog"
                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Información del producto</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="modal-body">

                            <form action="add_taller_user.php" method="post"
                                  name="form_add_talleres_users-<?php echo $row['id_producto']; ?>"
                                  id="form_add_talleres_users-<?php echo $row['id_producto']; ?>"
                                  class="needs-validation" novalidate>
                                <div class="estado">
                                    <label for="estado" class="form-label">Estado</label>
                                    <select name="estado" class="form-control" id="estado" required>
                                        <option value="1">Seleccione estado</option>
                                        <option value="pendiente subcont">Pendiente asignar</option>
                                        <option value="en fabricacion">en fabricacion</option>
                                        <option value="en pulimento">en pulimento</option>
                                        <option value="en carga">en carga</option>
                                        <option value="terminado">terminado</option>
                                    </select>
                                </div>
                                <div class="id_taller">
                                    <?php
                                    $tallerData = $db->rawQuery("SELECT id_taller,nombre_taller FROM talleres");
                                    ?>
                                    <label for="id_taller" class="form-label">Taller</label>
                                    <select name="id_taller" class="form-control" id="id_taller" required>
                                        <option>Seleccione taller</option>
                                        <?php foreach ($tallerData as $taller): ?>
                                            <option value="<?= $taller['id_taller'] ?>"><?= $taller['nombre_taller'] ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <input type="hidden" name="id_producto" value="<?php echo $row['id_producto']; ?>">
                                <br>
                                <div class="confirmar">
                                    <button type="submit" class="btn btn-primary">Confirmar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
<script>
    $(document).ready(function () {
        // Setup - add a text input to each footer cell
        $('#table-producto thead tr')
            .clone(true)
            .addClass('filters')
            .appendTo('#table-producto thead');

        var table = $('#table-producto').DataTable({
            "scrollX": true,
            "language": {
                "info": "Página _PAGE_ de _PAGES_",
                "search": "Buscar",
                "lengthMenu": "Mostrar _MENU_ registros",
                "infoEmpty": "Sin registros",
                "paginate": {
                    "previous": "Primero",
                    "next": "Último"
                },
                "infoFiltered": " - filtrado en _MAX_ registros",
                "zeroRecords": "No se encontraron resultados al filtrar"
            },
            orderCellsTop: true,
            fixedHeader: true,
            initComplete: function () {
                var api = this.api();

                // For each column
                api
                    .columns()
                    .eq(0)
                    .each(function (colIdx) {
                        // Set the header cell to contain the input element
                        var cell = $('.filters th').eq(
                            $(api.column(colIdx).header()).index()
                        );
                        var title = $(cell).text();
                        $(cell).html('<input type="text" placeholder="Buscar" />');

                        // On every keypress in this input
                        $(
                            'input',
                            $('.filters th').eq($(api.column(colIdx).header()).index())
                        )
                            .off('keyup change')
                            .on('keyup change', function (e) {
                                e.stopPropagation();

                                // Get the search value
                                $(this).attr('title', $(this).val());
                                var regexr = '({search})'; //$(this).parents('th').find('select').val();

                                var cursorPosition = this.selectionStart;
                                // Search the column for that value
                                api
                                    .column(colIdx)
                                    .search(
                                        this.value != ''
                                            ? regexr.replace('{search}', '(((' + this.value + ')))')
                                            : '',
                                        this.value != '',
                                        this.value == ''
                                    )
                                    .draw();

                                $(this)
                                    .focus()[0]
                                    .setSelectionRange(cursorPosition, cursorPosition);
                            });
                    });
            },
        });
    });
</script>

<style>
    thead input {
        width: 100%;
    }
</style>
<!-- //Main container -->
<?php include BASE_PATH . '/includes/footer.php'; ?>
