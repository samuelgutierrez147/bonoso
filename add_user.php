<?php
session_start();
require_once 'config/config.php';
require_once BASE_PATH . '/includes/auth_validate.php';

// Users class
require_once BASE_PATH . '/lib/Users/Users.php';
$users = new Users();
$edit = false;

// Serve POST request
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    // Sanitize input post if we want
    $data_to_db = filterDataPost();

    // Check whether the user name already exists
    $db = getDbInstance();
    $db->where('user_name', $data_to_db['user_name']);
    $db->get('users');

    if ($db->count >= 1) {
        $_SESSION['failure'] = 'Usuario ya existe';
        header('location: add_user.php');
        exit;
    }

    // Encrypting the password
    $data_to_db['password'] = password_hash($data_to_db['password'], PASSWORD_DEFAULT);
    // Reset db instance
    $db = getDbInstance();
    try {
        $last_id = $db->insert('users', $data_to_db);
        if ($last_id) {
            $_SESSION['success'] = 'Usuario creado correctamente';
            header('location: users.php');
            exit;
        }
    } catch (Exception $e) {
        $_SESSION['failure'] = $e->getMessage();
    }
}
?>
<?php include BASE_PATH . '/includes/header.php'; ?>
<div class="container" style="margin-top: 15px;">
    <div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">Crear usuario</h2>
        </div>
    </div>
    <?php include BASE_PATH . '/includes/flash_messages.php'; ?>
    <form class="well form-horizontal" action="" method="post" id="contact_form" enctype="multipart/form-data">
        <?php include BASE_PATH . '/forms/users_form.php'; ?>
    </form>
</div>
<?php include BASE_PATH . '/includes/footer.php'; ?>
