<?php
session_start();
require_once 'config/config.php';
require_once BASE_PATH . '/includes/auth_validate.php';
require_once BASE_PATH . '/lib/Users/Users.php';
$users = new Users();
// current page
$page = filter_input(INPUT_GET, 'page');
if (!$page) {
    $page = 1;
}

// Get DB instance. i.e instance of MYSQLiDB Library
$db = getDbInstance();
$id_user = filter_input(INPUT_GET, 'id_user', FILTER_VALIDATE_INT);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    // Sanitize input post if we want
    $data_to_db = filterDataPost();
    $permisos = implode(",", $data_to_db['permisos']);

    $db->where('id_user', $id_user);
    $stat = $db->update('users', ['permisos' => $permisos]);

    if ($stat) {
        $_SESSION['success'] = 'Permisos actualizados';
    } else {
        $_SESSION['failure'] = 'Algo falló al actualizar : ' . $db->getLastError();
    }

    header('location: users.php');
    exit;
}

$db->where('id_user', $id_user);
$permisos = $db->get('users', null,'permisos');
$permisos = reset($permisos);
$explodePermisos = explode(',',$permisos['permisos']);

?>
<?php include BASE_PATH . '/includes/header.php'; ?>
<!-- Main container -->
<div class="col-md-6 mx-auto">
    <div class="card">
        <div class="card-header text-center bg-primary text-white">
            Asignar Permisos
        </div>
        <div class="card-body text-capitalize text-center">
            <form id="form-privilegios" method="post" action="">
                <label for="">Añadir</label>
                <input type="checkbox" name="permisos[]"
                       value="add" <?php if(in_array('add',$explodePermisos)): echo 'checked'; endif;?>>
                <label for="">Editar</label>
                <input type="checkbox" name="permisos[]"
                       value="edit" <?php if(in_array('edit',$explodePermisos)): echo 'checked'; endif;?>>
                <label for="">Borrar</label>
                <input type="checkbox" name="permisos[]"
                       value="delete" <?php if(in_array('delete',$explodePermisos)): echo 'checked'; endif;?>>
                <div>
                    <button type="submit" class="btn btn-primary">Confirmar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<style>
    input[type="checkbox"] {
        position: relative;
        width: 60px;
        height: 30px;
        -webkit-appearance: none;
        background: rgb(168, 168, 168);
        outline: none;
        border-radius: 15px;
        box-shadow: inset 0 0 5px rgba(0, 0, 0, .5);
    }

    input:checked[type="checkbox"] {
        background: chocolate;
    }

    input[type="checkbox"]:before {
        content: "";
        position: absolute;
        width: 30px;
        height: 30px;
        border-radius: 20px;
        top: 0;
        left: 0;
        background: white;
        transition: 0.5s;

    }

    input:checked[type="checkbox"]:before {
        left: 30px;
    }
</style>
<!-- //Main container -->
<?php include BASE_PATH . '/includes/footer.php'; ?>
