<?php
session_start();
if (!$_SESSION['is_admin']) {
    $_SESSION['failure'] = 'No tiene acceso';
    header('location: index.php');
    exit;
}
require_once 'config/config.php';
require_once BASE_PATH . '/includes/auth_validate.php';

// User ID for which we are performing operation
$config_id = filter_input(INPUT_GET, 'config_id');
$operation = filter_input(INPUT_GET, 'operation', FILTER_SANITIZE_STRING);


($operation == 'edit') ? $edit = true : $edit = false;

// Serve POST request
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    // Sanitize input post if we want
    $data_to_db = filterDataPost();
    $db = getDbInstance();

    $config_id = filter_input(INPUT_GET, 'config_id', FILTER_VALIDATE_INT);

    // Reset db instance
    $db = getDbInstance();
    $db->where('config_id', $config_id, '=');
    $stat = $db->update('config_data', $data_to_db);

    if ($stat) {
        $_SESSION['success'] = 'Config <b>' . $data_to_db['name'] . '</b> actualizada';
    } else {
        $_SESSION['failure'] = 'Algo falló al actualizar la config: ' . $db->getLastError();
    }

    header('location: config.php');
    exit;
}

// Select where clause
$db = getDbInstance();
$db->where('config_id', $config_id);
$configData = $db->getOne("config_data");
?>
<?php include BASE_PATH . '/includes/header.php'; ?>
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Editar <b><?= $configData['name'] ?></b></h1>
    </div>

    <form class="well form-horizontal" action="" method="post" id="config_form" enctype="multipart/form-data">
        <?php include BASE_PATH . '/forms/config_form.php'; ?>
    </form>
</div>
<?php include BASE_PATH . '/includes/footer.php'; ?>
