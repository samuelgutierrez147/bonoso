<?php

class Users
{
    public function __construct()
    {
    }

    /**
     * @param $userId
     * @return array
     * @throws Exception
     */

    public function permisos ($id_user) {
        if(empty($_SESSION['success'])) {
            header("location: inicio.php");
        }
        $data = $this ->model->getPermisos();
        $data['id_user'] = $id_user;
        $this->views->getView($this, "permisos", $data);

    }

    public function getDataByUser($userId): array
    {
        $db = getDbInstance();
        $db->where('id_user', $userId, '=');

        if ($dataRow = $db->getOne('users')) {
            return $dataRow;
        }
        return [];
    }
}
