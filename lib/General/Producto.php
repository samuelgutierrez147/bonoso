<?php


class Producto
{
    public function __construct()
    {
    }

    /**
     * @param $IDhoras
     * @return array
     * @throws Exception
     */
    public function getDataByPruebaId($IDhoras): array
    {
        $db = getDbInstance();
        $db->where('id_horas', '=');

        if ($dataRow = $db->getOne('horas_produccion')) {
            return $dataRow;
        }
        return [];
    }

    public function getProductoByPedido($pedcliente): string
    {
        $db = getDbInstance();
        $db->where('num_pedido_cliente', $pedcliente, '=');

        if ($dataRow = $db->getOne('producto', 'producto')) {
            return $dataRow['producto'];

        }
        return '';
    }

    public function getNameByTallerId($tallerId): string
    {
        $db = getDbInstance();
        $db->where('id_taller', $tallerId, '=');

        if ($dataRow = $db->getOne('talleres', 'nombre_taller')) {
            return $dataRow['nombre_taller'];

        }
        return '';
    }
    public function getNameByHorasID($horasId): string
    {
        $db = getDbInstance();
        $db->where('id_horas', $horasId, '=');

        if ($dataRow = $db->getOne('horas_produccion', 'horas_fabrica')) {
            return $dataRow['horas_fabrica'];

        }
        return '';
    }

    public function getNameByPulimentoId($pulimentoId): string
    {
        $db = getDbInstance();
        $db->where('id_pulimento', $pulimentoId, '=');

        if ($dataRow = $db->getOne('pulimento', 'taller_pulimento')) {
            return $dataRow['taller_pulimento'];

        }
        return '';
    }

    public function getNameByComplementoId($complementoId): string
    {
        $db = getDbInstance();
        $db->where('id_taller_complemento', $complementoId, '=');

        if ($dataRow = $db->getOne('taller_complemento', 'taller_complemento')) {
            return $dataRow['taller_complemento'];

        }
        return '';
    }
    public function getNameByObraId($obraId): string
    {
        $db = getDbInstance();
        $db->where('id_obra', $obraId, '=');

        if ($dataRow = $db->getOne('obra', 'obra')) {
            return $dataRow['obra'];

        }
        return '';
    }

    public function getProductById($productId): array
    {
        $db = getDbInstance();
        $db->where('id_producto', $productId, '=');

        if ($dataRow = $db->getOne('producto')) {
            return $dataRow;
        }
        return [];
    }
}