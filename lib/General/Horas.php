<?php


class Horas
{
    public function __construct()
    {
    }

    /**
     * @param $IDhoras
     * @return array
     * @throws Exception
     */
    public function getDataByPruebaId($IDhoras): array
    {
        $db = getDbInstance();
        $db->where('id_horas', '=');

        if ($dataRow = $db->getOne('horas_produccion')) {
            return $dataRow;
        }
        return [];
    }

    public function getNameByProductId($productId): string
    {
        $db = getDbInstance();
        $db->where('id_producto', $productId, '=');

        if ($dataRow = $db->getOne('producto', 'producto')) {
            return $dataRow['producto'];

        }
        return '';
    }
    public function getProductById($productId): array
    {
        $db = getDbInstance();
        $db->where('id_producto', $productId, '=');

        if ($dataRow = $db->getOne('producto')) {
            return $dataRow;
        }
        return [];
    }

}