<?php


class Talleres
{
    public function __construct()
    {
    }

    /**
     * @param $IDhoras
     * @return array
     * @throws Exception
     */
    public function getDataByPruebaId($IDhoras): array
    {
        $db = getDbInstance();
        $db->where('id_taller', '=');

        if ($dataRow = $db->getOne('talleres')) {
            return $dataRow;
        }
        return [];
    }

    public function getProductById($productId): array
    {
        $db = getDbInstance();
        $db->where('id_taller', $productId, '=');

        if ($dataRow = $db->getOne('talleres')) {
            return $dataRow;
        }
        return [];
    }

}