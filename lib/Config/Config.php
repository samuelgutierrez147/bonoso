<?php

class Config
{
    public function getConfigValueByPath($path)
    {
        $db = getDbInstance();
        $db->where('path', $path, '=');

        if ($dataRow = $db->getOne('config_data','value')) {
            return $dataRow['value'];
        }
        return [];
    }
}
