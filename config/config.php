<?php

//Note: This file should be included first in every php page.
error_reporting(E_ALL);
ini_set('display_errors', 'On');
define('BASE_PATH', dirname(dirname(__FILE__)));
define('APP_FOLDER', 'simpleadmin');
define('CURRENT_PAGE', basename($_SERVER['REQUEST_URI']));

require_once BASE_PATH . '/lib/MysqliDb/MysqliDb.php';
require_once BASE_PATH . '/helpers/helpers.php';

if (!empty($_SESSION)) {
    $permisos = [];
    if(isset($_SESSION['permisos'])){
        $permisos = explode(',',$_SESSION['permisos']);
    }
    if (!empty($permisos) && !$_SESSION['is_admin']) {
        $uri = $_SERVER['REQUEST_URI'];
        $explodeUri = explode('/', $uri);
        $lastFile = end($explodeUri);
        //ocultar botones
        $allow = 0;
        foreach ($permisos as $permiso) {
            if (substr($lastFile, 0, strlen($permiso)) == $permiso) {
                $allow = 1;
                break;
            }
        }

        if(!$allow && (startsWith($lastFile,'add') || startsWith($lastFile,'edit') || startsWith($lastFile,'delete'))){
            header('Location: obra.php');
            exit;
        }
    }
}
/*
|--------------------------------------------------------------------------
| DATABASE CONFIGURATION
|--------------------------------------------------------------------------
 */

define('DB_HOST', "localhost");
define('DB_USER', "root");
define('DB_PASSWORD', "");
define('DB_NAME', "topsystem_bonoso");

/**
 * Get instance of DB object
 */
function getDbInstance()
{
    return new MysqliDb(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
}
