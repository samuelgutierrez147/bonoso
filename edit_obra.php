<?php
//Inicio sesion
session_start();
//Acceso a la configuracion
require_once 'config/config.php';
//Clase validadora
require_once BASE_PATH . '/includes/auth_validate.php';

//class
require_once BASE_PATH . '/lib/General/Producto.php';
$obra = new Producto();

//Llamos a un valor externo, el get se recoge a traves de la url PARAMETROS DE LA URL
$id_obra = filter_input(INPUT_GET, 'id_obra');
$operation = filter_input(INPUT_GET, 'operation', FILTER_SANITIZE_STRING);

($operation == 'edit') ? $edit = true : $edit = false;

// Serve POST request
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    // Sanitize input post if we want
    $data_to_db = filterDataPost();
    $db = getDbInstance();
    if(!isset($data_to_db['show_obra'])){
        $data_to_db['show_obra'] = 0;
    }
    $productsExplode = implode(',',$data_to_db['productos_obra']);
    $data_to_db['productos_obra'] = $productsExplode;
    $db->where('id_obra', $id_obra, '=');
    $stat = $db->update("obra", $data_to_db);

    // 3.1 si la obra se ha editado correctamente, paso 4
    if ($stat) {
        $_SESSION['success'] = 'Obra<b> con el ID ' . $id_obra . '</b> actualizado';
    } else {
        $_SESSION['failure'] = 'Algo falló al actualizar la obra: ' . $db->getLastError();
    }
    header('location: edit_obra.php?id_obra='.$id_obra.'&operation=edit');
    exit;

}

$db = getDbInstance();
$db->where('id_obra', $id_obra);
$edit_obra = $db->getOne("obra");
?>
<?php include BASE_PATH . '/includes/header.php'; ?>
<!-- Begin Page Content -->
<div class="container-fluid">
    <?php include BASE_PATH . '/includes/flash_messages.php'; ?>
    <?php include BASE_PATH . '/forms/obra_form.php'; ?>
</div>
<?php include BASE_PATH . '/includes/footer.php'; ?>
