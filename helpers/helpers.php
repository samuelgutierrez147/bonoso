<?php
require_once BASE_PATH . '/lib/Config/Config.php';

$reboseGraficaTaller = [];
$mesReboseGraficaTaller = [];

$reboseGraficaHorasAsignadas = 0;
$mesReboseGraficaHorasAsignadas = 0;
$anioReboseGraficaHorasAsignadas = 0;

/**
 * Function to generate random string.
 * @param $n
 * @return string
 */
function randomString($n): string
{

    $generated_string = "";

    $domain = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

    $len = strlen($domain);

    // Loop to create random string
    for ($i = 0; $i < $n; $i++) {
        // Generate a random index to pick characters
        $index = rand(0, $len - 1);

        // Concatenating the character
        // in resultant string
        $generated_string = $generated_string . $domain[$index];
    }

    return $generated_string;
}

/**
 * @return string
 */
function getSecureRandomToken(): string
{
    return bin2hex(openssl_random_pseudo_bytes(16));
}

/**
 * Clear Auth Cookie
 */
function clearAuthCookie()
{

    unset($_COOKIE['series_id']);
    unset($_COOKIE['remember_token']);
    setcookie('series_id', null, -1, '/');
    setcookie('remember_token', null, -1, '/');
}

function getBaseUrl()
{
    // output: /myproject/index.php
    $currentPath = $_SERVER['PHP_SELF'];

    // output: Array ( [dirname] => /myproject [basename] => index.php [extension] => php [filename] => index )
    $pathInfo = pathinfo($currentPath);

    // output: localhost
    $hostName = $_SERVER['HTTP_HOST'];

    // output: http://
    $protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"], 0, 5)) == 'https' ? 'https' : 'http';

    // return: http://localhost/myproject/
    return $protocol . '://' . $hostName . $pathInfo['dirname'] . "/";
}

/**
 * Generate popup with data
 * @param $rowId
 * @param $section
 * @param $formAction
 * @return string
 */
function generatePopUpDelete($rowId, $section, $formAction): string
{
    return '
    <div class="modal fade" id="confirm-delete-' . $rowId . '" role="dialog">
        <div class="modal-dialog">
            <form action="' . $formAction . '" method="POST">
                <!-- Modal content -->
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="del_id" id="del_id" value="' . $rowId . '">
                        <p>¿Está seguro de borrar este ' . $section . '?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary">Aceptar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>';
}

function generatePopUpTrabajadores($rowId, $section, $formAction, $idUsers): string
{
    $db = getDbInstance();
    $trabajadoresData = $db->rawQuery("SELECT id_user, user_name FROM users WHERE user_type = 'trabajador'");
    $explodeUSers = explode(',', $idUsers);
    $options = '';
    foreach ($trabajadoresData as $trabajadores) {
        $selected = '';
        if (in_array($trabajadores['id_user'], $explodeUSers)) {
            $selected = 'selected';
        }
        $options .= "<option value=" . $trabajadores['id_user'] . " " . $selected . ">" . $trabajadores['user_name'] . "</option>";
    }
    $botonSubmit = '<button type="submit" class="btn btn-primary">Aceptar</button>';
    $selectTrabajadoresHtml = '<select name="trabajadores[]" class="form-control" id="trabajadores[]" multiple>
                            <option value="">Elija trabajador</option>
                            ' . $options . '
                        </select>';
    if ($options == '') {
        $selectTrabajadoresHtml = 'No existen trabajadores';
        $botonSubmit = '';
    }

    return '
    <div class="modal fade" id="trabajadores-' . $rowId . '" role="dialog">
        <div class="modal-dialog">
            <form action="' . $formAction . '" method="POST">
                <!-- Modal content -->
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                        ' . $selectTrabajadoresHtml . '
                        <input type="hidden" name="id_obra" id="id_obra" value="' . $rowId . '">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        '.$botonSubmit.'
                    </div>
                </div>
            </form>
        </div>
    </div>';
}

function showFullColumnsByTable($table)
{
    if ($table) {
        $db = getDbInstance();
        return $db->query("SHOW FULL COLUMNS FROM {$table}");
    } else {
        return [];
    }

}

function getColumnsFieldByTable($table)
{
    $columns = showFullColumnsByTable($table);
    $result = [];
    if (count($columns) > 0) {
        foreach ($columns as $column) {
            $result[] = $column['Field'];
        }
    }
    return $result;
}

function getStartAndEndDate($week, $year, $showYear = false)
{
    $dto = new DateTime();
    $dto->setISODate($year, $week);
    $format = ($showYear) ? 'd-m-Y' : 'd-m';
    $ret['week_start'] = $dto->format($format);
    $dto->modify('+6 days');
    $ret['week_end'] = $dto->format($format);
    return $ret;
}

function filterDataPost()
{
    $data_to_db = filter_input_array(INPUT_POST);
    foreach ($data_to_db as &$data) {
        if ($data == '') {
            $data = 0;
        }
        if ($data == 'on') {
            $data = 1;
        }
    }

    return $data_to_db;
}

function getWeeksFromMonthAndYear($month, $anio = null)
{
    $year = ($anio != null) ? $anio : date('Y');
    $date = $year . '-' . $month . '-01';
    $lastDayDate = date('t', strtotime($date));
    $weeks = [];
    $semanaActual = (int)date('W', strtotime(date('Y-m-d')));
    for ($i = 1; $i <= $lastDayDate; $i++) {
        $dateFormat = strtotime($i . '-' . $month . '-' . $year);
        if (!in_array(intval(date('W', $dateFormat)), $weeks)) {
            array_push($weeks, intval(date('W', $dateFormat)));
        }
    }
    if (in_array($semanaActual, $weeks)) {
        $countElements = count($weeks);
        array_splice($weeks, 0, (array_search($semanaActual, $weeks) - $countElements));
    }

    return implode(',', $weeks);
}

function getPedidosYaAsignados($mes, $tallerId = null, $anio = null, $extra = 0)
{
    $db = getDbInstance();
    $anioConsulta = ($anio != null) ? $anio : date('Y');
    $meses = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    $sqlTaller1 = '';
    $sqlTaller2 = '';
    if ($tallerId != null) {
        $sqlTaller1 = " p.id_taller = {$tallerId} AND";
        $sqlTaller2 = " t.id_taller = {$tallerId} AND";
    }
    $mesInt = array_search($mes, $meses) + 1;
    $firstDate = date('Y-m-d', strtotime($anioConsulta . '-' . $mesInt . '-01'));
    $lastWeekFirstDate = date('Y-m-d', strtotime($firstDate . '+ 6 days'));
    $lastDate = date('Y-m-t', strtotime($firstDate));
    $firstWeekDate = date('Y-m-d', strtotime($lastDate . '- 6 days'));

    $horasAsignadasPedidos = $db->rawQuery("SELECT 
        SUM(hp.horas_fabrica) as horas_asignadas
        FROM producto p
        INNER JOIN fabricacion f ON f.id_fabricacion = p.id_fabricacion
        INNER JOIN horas_produccion hp ON p.id_horas = hp.id_horas 
        WHERE " . $sqlTaller1 . " f.fecha_ped_compra_fabricacion BETWEEN '{$firstDate}' AND '{$lastDate}'
        AND YEAR(f.fecha_ped_compra_fabricacion) = '" . $anioConsulta . "'
        AND p.estado = 'en fabricacion'
        AND f.fecha_terminado IS NULL
        GROUP BY MONTH(f.fecha_ped_compra_fabricacion)");


    $semanasMes = getWeeksFromMonthAndYear($mesInt, $anioConsulta);
    $semanaUltima = explode(',', $semanasMes);
    $semanaPrimera = reset($semanaUltima);
    $semanaUltima = end($semanaUltima);

    $horasCapacidadPedidos = $db->rawQuery("
        SELECT 
            SUM(s.horas_trabajo_dia * s.dias_trabajo_semana * s.num_trabajadores) as horas_capacidad
        FROM semanas_taller s
        INNER JOIN talleres t ON s.id_taller = t.id_taller
        WHERE " . $sqlTaller2 . " s.semana IN ({$semanasMes})
        AND s.anio = '" . $anioConsulta . "'");

    $horasAsignadasPedidos = reset($horasAsignadasPedidos);
    $horasCapacidadPedidos = reset($horasCapacidadPedidos);

    $horasCapacidad = round(($horasCapacidadPedidos['horas_capacidad']) ?? 0, 2);
    $horasAsignadas = round(($horasAsignadasPedidos['horas_asignadas'] + $extra) ?? 0, 2);

    $porcentaje = ($horasAsignadas > 0 && $horasCapacidad > 0) ? round(($horasAsignadas / $horasCapacidad) * 100) : 0;
    $porcentaje = ($porcentaje > 99) ? 100 : $porcentaje;
    $restante = 100 - $porcentaje;

    return [
        'horas_capacidad' => $horasCapacidad,
        'horas_asignadas' => $horasAsignadas,
        'porcentaje' => number_format((float)$porcentaje, 2, '.', ''),
        'restante' => number_format((float)abs($restante), 2, '.', '')
    ];
}

function getPedidosYaAsignadosMasPendientes($mes, $anio = null, $extra = 0)
{
    $db = getDbInstance();
    $anioConsulta = ($anio != null) ? $anio : date('Y');
    $horasAsignadasPedidosMasPendientes = $db->rawQuery("
        SELECT 
        SUM(hp.horas_fabrica) as horas_asignadas
        FROM producto p
        INNER JOIN fabricacion f ON f.id_fabricacion = p.id_fabricacion
        INNER JOIN horas_produccion hp ON p.id_horas = hp.id_horas 
        WHERE MONTH(f.fecha_ped_compra_fabricacion) = '" . date("m", strtotime($mes)) . "'
        AND YEAR(f.fecha_ped_compra_fabricacion) = '" . $anioConsulta . "'
        AND (p.estado = 'en fabricacion'
        OR p.estado = 'pendiente subcont')
        AND f.fecha_terminado IS NULL
        GROUP BY MONTH(f.fecha_ped_compra_fabricacion)");

    $semanasMes = getWeeksFromMonthAndYear($mes, $anioConsulta);

    $horasCapacidadPedidosMasPendientes = $db->rawQuery("
        SELECT 
            SUM(s.horas_trabajo_dia * s.dias_trabajo_semana * s.num_trabajadores) as horas_capacidad
        FROM semanas_taller s
        INNER JOIN talleres t ON s.id_taller = t.id_taller
        WHERE s.semana IN ({$semanasMes})
        AND s.anio = '" . $anioConsulta . "'");

    $horasAsignadasPedidosMasPendientes = reset($horasAsignadasPedidosMasPendientes);
    $horasCapacidadPedidosMasPendientes = reset($horasCapacidadPedidosMasPendientes);

    $horasCapacidadPendiente = ($horasCapacidadPedidosMasPendientes['horas_capacidad']) ?? 0;
    $horasAsignadasPendiente = ($horasAsignadasPedidosMasPendientes['horas_asignadas'] + $extra) ?? 0;

    $porcentajePendiente = ($horasAsignadasPendiente && $horasCapacidadPendiente) ? round(($horasAsignadasPendiente / $horasCapacidadPendiente) * 100) : 0;
    $restantePendiente = 100 - $porcentajePendiente;

    return [
        'horas_asignadas' => $horasAsignadasPendiente,
        'horas_capacidad' => $horasCapacidadPendiente,
        'porcentaje' => number_format((float)$porcentajePendiente, 2, '.', ''),
        'restante' => number_format((float)abs($restantePendiente), 2, '.', ''),
    ];
}

function getDatosMesTaller($mes, $anio = null, $isAnterior = false)
{
    $db = getDbInstance();
    $anioConsulta = ($anio != null) ? $anio : date('Y');
    $datosGraficaTalleres = [];
    $datosAsignadasTallerQuery = $db->rawQuery("SELECT
           SUM(hp.horas_fabrica) as horas_asignadas,
           p.id_taller as id_taller
        FROM producto p
        INNER JOIN horas_produccion hp ON hp. id_horas = p.id_horas 
        INNER JOIN talleres t ON t.id_taller = p.id_taller
        INNER JOIN fabricacion f ON p.id_fabricacion = f.id_fabricacion
        WHERE MONTH(f.fecha_ped_compra_fabricacion) = '" . date("m", strtotime($mes)) . "'
        AND YEAR(f.fecha_ped_compra_fabricacion) = '" . $anioConsulta . "'
        AND p.estado = 'en fabricacion' 
        AND f.fecha_terminado IS NULL
        GROUP BY t.id_taller");

    $semanasMes = getWeeksFromMonthAndYear($mes);

    $datosCapacidadTallerQuery = $db->rawQuery("SELECT 
        s.horas_trabajo_dia as horas_capacidad,
        s.dias_trabajo_semana as dias_semana,
        t.id_taller as id_taller,
        s.num_trabajadores as num_trabajadores
        FROM semanas_taller s
        INNER JOIN talleres t ON s.id_taller = t.id_taller
        AND s.semana IN ({$semanasMes})
        AND s.anio = '" . $anioConsulta . "'");

    $datosTalleres = $db->rawQuery("SELECT id_taller FROM talleres");

    if (!empty($datosAsignadasTallerQuery)) {
        $talleresIds = array_column($datosAsignadasTallerQuery, 'id_taller');
        foreach ($datosAsignadasTallerQuery as $datosAsignadas) {
            $datosGraficaTalleres[$datosAsignadas['id_taller']]['horas_asignadas'] = ($isAnterior) ? 0 : $datosAsignadas["horas_asignadas"];
            $datosGraficaTalleres[$datosAsignadas['id_taller']]['horas_capacidad'] = 0;
        }
        foreach ($datosTalleres as $taller){
            if(!in_array($taller['id_taller'],$talleresIds)){
                $datosGraficaTalleres[$taller['id_taller']]['horas_asignadas'] = 0;
                $datosGraficaTalleres[$taller['id_taller']]['horas_capacidad'] = 0;
            }
        }
    } else {
        foreach ($datosTalleres as $taller) {
            $datosGraficaTalleres[$taller['id_taller']]['horas_asignadas'] = 0;
            $datosGraficaTalleres[$taller['id_taller']]['horas_capacidad'] = 0;
        }
    }

    if (!empty($datosCapacidadTallerQuery)) {
        foreach ($datosCapacidadTallerQuery as $datosCapacidad) {
            if (array_key_exists($datosCapacidad['id_taller'], $datosGraficaTalleres)) {
                $datosGraficaTalleres[$datosCapacidad['id_taller']]['horas_capacidad'] += ($isAnterior) ? 0 : ($datosCapacidad['horas_capacidad'] * $datosCapacidad['dias_semana'] * $datosCapacidad['num_trabajadores']);
            }
        }
    }

    return $datosGraficaTalleres;
}

function getPedidosYaAsignadosMesGrafica($mes, $anio = null, $isAnterior = false)
{
    $db = getDbInstance();
    $anioConsulta = ($anio != null) ? $anio : date('Y');
    $datosGraficaTalleres = [];
    $talleres = $db->get('talleres');
    foreach ($talleres as $taller) {
        if ($isAnterior) {
            $datosGraficaTalleres[$taller['id_taller']] = [
                'horas_capacidad' => 0,
                'horas_asignadas' => 0,
                'porcentaje' => 0,
                'restante' => 100
            ];
        } else {
            $datosGraficaTalleres[$taller['id_taller']] = getPedidosYaAsignados($mes, $taller['id_taller'], $anioConsulta);
        }
    }

    return $datosGraficaTalleres;
}

/**
 * @param $datosGraficaHorasAsignadasPorcentaje
 * @param $datosGraficaHorasAsignadasRestantes
 * @param $datosGraficaHorasPendientesPorcentaje
 * @param $datosGraficaHorasPendientesRestantes
 * @param $datosGraficaTalleres
 * @param $datosGraficaTallerGrafica
 * @return int
 */
function applyDataAnualByDate(&$datosGraficaHorasAsignadasPorcentaje,
                              &$datosGraficaHorasAsignadasRestantes,
                              &$datosGraficaHorasPendientesPorcentaje,
                              &$datosGraficaHorasPendientesRestantes,
                              &$datosGraficaTalleres,
                              &$datosGraficaTallerGrafica,
                              &$datosGraficaHorasAsignadasAsignadas,
                              &$datosGraficaHorasAsignadasCapacidad,
                              &$datosGraficaHorasPendientesAsignadas,
                              &$datosGraficaHorasPendientesCapacidad)
{
    $mesActual = (int)date('m');
    $anioActual = (int)date('Y');
    $meses = [];
    $tipo = 0;
    if ($mesActual == 1 || $mesActual == 2) {
        //sacamos datos del año anterior a este (noviembre y diciembre)
        $meses = [-1 => 'November', -2 => 'December'];
        $anioActual--;

    } elseif ($mesActual == 12 || $mesActual == 11) {
        //sacamos datos del año posterior a este (enero y febrero)
        $meses = [12 => 'January', 13 => 'February'];
        $anioActual++;
        $tipo = 1;
    }

    if (!empty($meses)) {
        foreach ($meses as $mes) {
            $pedidosYaAsignados = getPedidosYaAsignados($mes, null, $anioActual);
            $typeArray = ($tipo == 0) ? 'array_unshift' : 'array_push';

            $typeArray($datosGraficaHorasAsignadasPorcentaje, $pedidosYaAsignados['porcentaje']);
            $typeArray($datosGraficaHorasAsignadasRestantes, $pedidosYaAsignados['restante']);
            $typeArray($datosGraficaHorasAsignadasAsignadas, $pedidosYaAsignados['horas_asignadas']);
            $typeArray($datosGraficaHorasAsignadasCapacidad, $pedidosYaAsignados['horas_capacidad']);

            // ---------------------------------------------------------------------------------------------------//
            $pedidosAsignadosMasPendientes = getPedidosYaAsignadosMasPendientes($mes, $anioActual);
            $typeArray($datosGraficaHorasPendientesPorcentaje, $pedidosAsignadosMasPendientes['porcentaje']);
            $typeArray($datosGraficaHorasPendientesRestantes, $pedidosAsignadosMasPendientes['restante']);
            $typeArray($datosGraficaHorasPendientesAsignadas, $pedidosAsignadosMasPendientes['horas_asignadas']);
            $typeArray($datosGraficaHorasPendientesCapacidad, $pedidosAsignadosMasPendientes['horas_capacidad']);

            // ---------------------------------------------------------------------------------------------------//
            $typeArray($datosGraficaTalleres, getDatosMesTaller($mes, $anioActual));

            // ---------------------------------------------------------------------------------------------------//
            $typeArray($datosGraficaTallerGrafica, getPedidosYaAsignadosMesGrafica($mes, $anioActual));
        }
        return $tipo;
    }
    return 2;
}

function applyReboseGraficaTaller(&$datosGraficaTallerGrafica, $tipo)
{
    $db = getDbInstance();
    global $reboseGraficaTaller;
    global $mesReboseGraficaTaller;
    global $anioReboseGraficaTaller;
    $anioActual = (int)date('Y');
    $anio = $anioActual;
    $mesesCogidosPorTipo = [];

    switch ($tipo) {
        case 1: //2 posteriores
            $anio++;
            $mesesCogidosPorTipo = [12, 13];
            break;
        case 0: //2 anteriores
            $anio--;
            $mesesCogidosPorTipo = [0, 1];
            break;
    }

    //Calculo rebose por taller
    foreach ($datosGraficaTallerGrafica as $mes => $graficaTaller) {
        $mesInt = $mes + 1;
        $semanasMes = getWeeksFromMonthAndYear($mes, $anioActual);
        if (in_array($mes, $mesesCogidosPorTipo)) {
            if ($tipo == 1) {
                if ($mes == 12)
                    $mesInt = 1;
                elseif ($mes == 13)
                    $mesInt = 2;
            } elseif ($tipo == 0) {
                if ($mes == 1)
                    $mesInt = 11;
                elseif ($mes == 2)
                    $mesInt = 12;
            }
            $semanasMes = getWeeksFromMonthAndYear($mesInt, $anio);
        }

        foreach ($graficaTaller as $tallerId => $datos) {
            $diferenciaHorasMes = $datos['horas_capacidad'] - $datos['horas_asignadas'];
            $reboseMes = ($datos['horas_asignadas'] > $datos['horas_capacidad']) ? abs($diferenciaHorasMes) : 0;

            if ($reboseMes > 0) {
                $reboseGraficaTaller[$tallerId] = 0;
                if (isset($reboseGraficaTaller[$tallerId]) && $reboseGraficaTaller[$tallerId] == 0) {
                    $mesReboseGraficaTaller[$tallerId] = $mes;
                    $anioReboseGraficaTaller[$tallerId] = $anioActual;
                }
                $reboseGraficaTaller[$tallerId] += $reboseMes;
            }
        }
    }
}

function processData(&$datosGraficaHorasAsignadasPorcentaje,
                     &$datosGraficaHorasAsignadasRestantes,
                     &$datosGraficaHorasAsignadasAsignadas,
                     &$datosGraficaHorasAsignadasCapacidad,
                     $tipo)
{
    global $mesReboseGraficaHorasAsignadas;
    global $reboseGraficaHorasAsignadas;

    $anioActual = (int)date('Y');
    $anio = $anioActual;
    $mesesCogidosPorTipo = [];

    if ($reboseGraficaHorasAsignadas) {
        switch ($tipo) {
            case 1: //2 posteriores
                $anio++;
                $mesesCogidosPorTipo = [12, 13];
                break;
            case 0: //2 anteriores
                $anio--;
                $mesesCogidosPorTipo = [0, 1];
                break;
        }

        //Ponemos mesrebose mes actual para que empiece por ahi
        $mesRebose = (int)date('m');

        if (!empty($mesesCogidosPorTipo)) {
            $mesRebose += 2;
            if ($tipo == 1) {
                if ($mesRebose == 12)
                    $mesRebose = 1;
                elseif ($mesRebose == 13)
                    $mesRebose = 2;
            } elseif ($tipo == 0) {
                if ($mesRebose == 1)
                    $mesRebose = 11;
                elseif ($mesRebose == 2)
                    $mesRebose = 12;
            }
        }

        $porcentajePendiente = 100;
        while ($porcentajePendiente >= 100) {
            $horasCapacidadMes = $datosGraficaHorasAsignadasCapacidad[$mesRebose];
            $horasAsignadasMes = $datosGraficaHorasAsignadasAsignadas[$mesRebose];
            $horasAsignadasMasRebose = $horasAsignadasMes + $reboseGraficaHorasAsignadas;

            $porcentajePendiente = round(($horasAsignadasMasRebose / $horasCapacidadMes) * 100);
            $porcentajePendiente = ($porcentajePendiente >= 100) ? 100 : $porcentajePendiente;
            $restantePendiente = abs(100 - $porcentajePendiente);

            $datosGraficaHorasAsignadasPorcentaje[$mesRebose] = number_format((float)$porcentajePendiente, 2, '.', '');
            $datosGraficaHorasAsignadasRestantes[$mesRebose] = number_format((float)$restantePendiente, 2, '.', '');
            $mesRebose++;

            $reboseGraficaHorasAsignadas = ($horasAsignadasMasRebose - $horasCapacidadMes) < 0 ? 0 : $horasAsignadasMasRebose - $horasCapacidadMes;
        }
    }
}

function processDataTaller(&$datosGraficaTallerGrafica,
                           $tipo)
{
    global $mesReboseGraficaTaller;
    global $reboseGraficaTaller;

    $anioActual = (int)date('Y');
    $anio = $anioActual;
    $mesesCogidosPorTipo = [];

    if (!empty($reboseGraficaTaller)) {
        switch ($tipo) {
            case 1: //2 posteriores
                $anio++;
                $mesesCogidosPorTipo = [12, 13];
                break;
            case 0: //2 anteriores
                $anio--;
                $mesesCogidosPorTipo = [0, 1];
                break;
        }

        foreach ($datosGraficaTallerGrafica as $mesIdentifier => &$datosByTaller) {
            foreach ($datosByTaller as $idTaller => &$datos) {

                if (!isset($mesReboseGraficaTaller[$idTaller]))
                    continue;

                //Ponemos mesrebose mes actual para que empiece por ahi
                $mesRebose = (int)date('m');

                if (!empty($mesesCogidosPorTipo)) {
                    $mesRebose += 2;
                    if ($tipo == 1) {
                        if ($mesRebose == 12)
                            $mesRebose = 1;
                        elseif ($mesRebose == 13)
                            $mesRebose = 2;
                    } elseif ($tipo == 0) {
                        if ($mesRebose == 1)
                            $mesRebose = 11;
                        elseif ($mesRebose == 2)
                            $mesRebose = 12;
                    }
                }

                if ($mesIdentifier != $mesRebose)
                    continue;

                $porcentajePendiente = 100;
                while ($porcentajePendiente >= 100) {
                    $horasCapacidadMes = $datosGraficaTallerGrafica[$mesRebose][$idTaller]['horas_capacidad'];
                    $horasAsignadasMes = $datosGraficaTallerGrafica[$mesRebose][$idTaller]['horas_asignadas'];
                    $horasAsignadasMasRebose = $horasAsignadasMes + $reboseGraficaTaller[$idTaller];

                    $porcentajePendiente = round(($horasAsignadasMasRebose / $horasCapacidadMes) * 100);
                    $porcentajePendiente = ($porcentajePendiente >= 100) ? 100 : $porcentajePendiente;
                    $restantePendiente = abs(100 - $porcentajePendiente);

                    $datosGraficaTallerGrafica[$mesRebose][$idTaller]['porcentaje'] = number_format((float)$porcentajePendiente, 2, '.', '');
                    $datosGraficaTallerGrafica[$mesRebose][$idTaller]['restante'] = number_format((float)$restantePendiente, 2, '.', '');

                    $mesRebose++;

                    $reboseGraficaTaller[$idTaller] = ($horasAsignadasMasRebose - $horasCapacidadMes) < 0 ? 0 : $horasAsignadasMasRebose - $horasCapacidadMes;
                }
            }
        }
    }
}

function startsWith($haystack, $needle)
{
    $length = strlen($needle);
    return (substr($haystack, 0, $length) === $needle);
}

function endsWith($haystack, $needle)
{
    $length = strlen($needle);
    if ($length == 0) {
        return true;
    }

    return (substr($haystack, -$length) === $needle);
}